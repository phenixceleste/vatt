<style>
img.icon{
    height:2em;vertical-align:middle;background:white;margin-right:3px;
    margin-left:3px;
}
</style>

# VATT

VATT is a 2D click-and-run game.
You must go from platform to platform by jumping.
You can collect coins while playing.
With 10 coins you can save the replay of your last game, to watch it multiple time.

You lose when you fall out of the world.
Your collection is kept when you lose.

<img src="doc/screenshot_playing.png" style="max-height:10em;" />


## Commands

* <img class="icon" src="doc/keys/esc.png" /> Pauses or resumes the game.

* <img class="icon" src="doc/keys/f11.png" /> Toggle fullscreen mode.

### In-game

* <img class="icon" src="doc/keys/space.png" style="padding: 0 1px;" /> 
  Jump.
    Maintain the key pressed to jump higher and further.
    You must be on a platform to initiate a jump.
<br/>

* <img class="icon" src="doc/keys/s.png" /> or <img class="icon" src="doc/keys/down.png" /> 
  Bend down. Bending down reduces your height by half. Release the key to stop bending down. You must be on a platform to start AND stop bending down.

### In the menu

* <img class="icon" src="doc/keys/enter.png" /> Click on the selected menu item.

* <img class="icon" src="doc/keys/z.png" /> or <img class="icon" src="doc/keys/w.png" /> or <img class="icon" src="doc/keys/up.png" /> 
  Select the previous menu item.

* <img class="icon" src="doc/keys/s.png" /> or <img class="icon" src="doc/keys/down.png" /> 
  Select the next menu item.

<img src="doc/screenshot_menu.png" style="max-height:10em;" />


## Compiling the code

**Dependency**: This project requires SFML 2.5.1.
(https://www.sfml-dev.org/license.php)

The project must be compiled using `make` tool.

* `make` compiles a release version of the code

* `make debug` compiles a debug version of the code. The debug version has the same features as the release version but adds debug messages in the code and information about the source files in the executable.

`make` and `make debug` produce an executable named "vatt" or "vatt.exe" at the root of the project.

* `make clean` cleans all the generated files (including the executable), leaving only the source code. 
It is not necessary to clean the project between two executions of the same `make` command, but the project must be cleaned before switching from a command to another (this applies to `make`, `make debug` and `make test`).

## Tests

* `make test` compiles a debug version with tests included. This command produces a non-playable executable named "vatt_test" or "vatt_test.exe" at the root of the project.

The name of the tests to execute must be given in the command line. If there are multiple tests, they must be space-separated.

It is possible to give an argument to a test by using the `=` char.

The `--fast` argument may be added to run the test as fast as possible. This will cause the
program to ignore delays between ticks without altering the virtual (in-game) tick interval.

Examples:
* `./vatt_test test1 test2`
* `./vatt_test test_name=argument`
* `./vatt_test test_name --fast`

Currently, the tests implemented are `jump` (no argument expected) and `plat_gen` (optional argument: the maximum time of the simulation)

For further information about what these tests do, check the documentation in `include/tests/TestJump.hpp` and `include/tests/TestPlatformGeneration.hpp`.

## Other make commands

* `make fmt` formats all the source files with clang. You do not have to use it unless you edit the code and want to push your changes on the GitLab.

## Documentation

Most methods are documented in the code (in the header files).
A doxygen documentation is also available at https://vatt-documentation.alwaysdata.net/.

Additionally, a PDF named "game_mechanics.pdf" is available in the directory `doc` and
explains the physics of the game and the platform generation method.

## Credits

The key icons used in this documentation file are taken from <a target="_blank" href="https://icons8.com">Icons8</a>.

<a target="_blank" href="https://icons8.com/icon/3Apt7KAlK2HU/space-key">Space Key</a>

<a target="_blank" href="https://icons8.com/icon/17794/esc">Esc Key</a>

<a target="_blank" href="https://icons8.com/icon/23352/enter-key">Enter Key</a>

<a target="_blank" href="https://icons8.com/icon/OUQq3hOZ2mcF/s-key">S Key</a> 

<a target="_blank" href="https://icons8.com/icon/e7T8GCKlORtw/w-key">W Key</a>

<a target="_blank" href="https://icons8.com/icon/l7U9yG7Ne51B/z-key">Z Key</a> 

<a target="_blank" href="https://icons8.com/icon/MxQ4e307CtvQ/down-arrow">Down Arrow</a>

<a target="_blank" href="https://icons8.com/icon/jWOSIpuUaulH/f11-key">F11 Key</a>
