// -*- lsst-c++ -*-

#ifndef _COIN_GENERATOR_HPP_
#define _COIN_GENERATOR_HPP_

#include "objects/Coin.hpp"

class GameManager;

/**
 * This class provides methods to generate coins on platforms and
 * trajectories.
 */
class CoinGenerator {

private:
  GameManager &m_board;

public:
  CoinGenerator(GameManager &gameManager);

  /**
   * This method generates randomly coins on the trajectory to the new platform
   * and on the new platform.
   */
  void generateCoin();
};

#endif
