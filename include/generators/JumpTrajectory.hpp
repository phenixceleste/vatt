// -*- lsst-c++ -*-

#ifndef _JUMP_TRAJECTORY_HPP_
#define _JUMP_TRAJECTORY_HPP_

#include "calc/PolynomCalc.hpp"
#include "calc/Quantity.hpp"
#include "components/SpeedModel.hpp"
#include <functional>

/**
 * This class provides methods to compute the (expected) trajectory of the
 * player between two platforms. Unlike what the name may suggest, this class is
 * also used for free-fall trajectories between two platforms.
 *
 * This class cannot be inherited.
 *
 * @see Read game_mechanics.pdf
 * for explanation of the computation of the trajectory and the physics of the
 * game.
 */
class JumpTrajectory final {

public:
  /// The type for functions that should return a random Time between the bounds
  /// given as arguments.
  using RandomProvider = std::function<const Time(const Time &, const Time &)>;

private:
  bool m_should_jump;
  Time m_T_init;
  Length m_x_0;
  Length m_y_0;
  Time m_t_0;
  Time m_T_p;

  /**
   * Return a random trajectory for a player who jumps from a platform at
   * height y_0 to a platform at height y. The random provider function
   * must be given by the caller.
   *
   * @param randQuantity
   * @param T_init The time when the jump begins
   * @param x_0,y_0 The position of the player when the jump begins
   * @param y The position of the platform to reach
   */
  static const JumpTrajectory forJump(const RandomProvider &randQuantity,
                                      const Time &T_init, const Length &x_0,
                                      const Length &y_0, const Length &y);
  /**
   * Return the (unique) trajectory of a player who falls from a
   * platform at height y_0 to a platform at height y.
   *
   * @param T_init The time when the fall begins
   * @param x_0,y_0 The position of the player when the fall begins
   * @param y The height of the platform to reach
   */
  static const JumpTrajectory forFreeFall(const Time &T_init, const Length &x_0,
                                          const Length &y_0, const Length &y);

  /**
   * The polynomial corresponding to the trajectory of the jump
   * before the player stops gliding.
   * It is sensible to evaluate this polynomial only with t between
   * 0 and the player's gliding time.
   */
  Polynomial3<0, 1, 1, 0> m_y_gliding;
  /**
   * Return the polynomial corresponding to the trajectory of the jump
   * after the player stopped gliding, assuming they have glided for
   * t_0 < T_max. It is sensible to evaluate this polynomial only with t >= t_0.
   */
  Polynomial2<0, 1, 1, 0> m_y_fall;

  static const Time computeT_1();

  /**
   * Get the duration of a free fall from a platform at height y_0
   * to a platform at height y.
   */
  static const Time getFallTime(const Length &y_0, const Length &y);

  /**
   * Take a random landing time for a player who jumps from a platform at height
   * y_0 to a platform at height y. The random provider function must be given
   * by the caller.
   */
  static const Time takeRandomLandingTime(const RandomProvider &randQuantity,
                                          const Length &y_0, const Length &y);

public:
  /**
   * Use the randomTrajectory instead, unless you know the parameters of the
   * trajectory.
   *
   * @param should_jump Whether the player should jump.
   * @param T_init The time when the player should begin to follow this
   * trajectory
   * @param x_0,y_0 The initial position of (the bottom-left corner of) the
   * player
   * @param t_0 The expected gliding time of the player. Ignored if should_jump
   * is false.
   * @param T_p The expected landing time of the player.
   */
  JumpTrajectory(bool should_jump, const Time &T_init, const Length &x_0,
                 const Length &y_0, const Time &t_0, const Time &T_p);

  /// The initial vertical speed of the jump.
  static const Speed V_JMP;
  /// The value of the gravity acceleration.
  static const Acceleration g;
  /// The initial acceleration due to gliding.
  /// @see game_mechanics.pdf
  static const Acceleration G_0;
  /// The maximum gliding time.
  static const Time T_max;

  /**
   * The polynomial that gives the height of a jump as a function of the gliding
   * time. The gliding time used to evaluate the polynomial must be no greater
   * than T_1, otherwise the result is unspecified.
   */
  static const Polynomial4<0, 1, 1, 0> JUMP_HEIGHT;

  /// The height of the smallest jump (without gliding)
  static const Length H_MIN;
  /// The height of the highest jump (when gliding for T_1)
  static const Length H_MAX;

  /**
   * T_1 is the minimum gliding time that allows to reach the maximum height of
   * a jump. If T_1 < T_max, gliding for more than T_1 may allow the player to
   * go further, but not higher.
   * @see game_mechanics.pdf
   */
  static const Time T_1;

  /**
   * Get the time the player is expected to glide for to jump from a platform at
   * height y_0 to a platform at height y in T_p seconds.
   */
  static const Time getExpectedGlidingTime(const Time &T_p, const Length &y_0,
                                           const Length &y);

  /**
   * Get the minimum time after which a jumping player may expect to reach
   * the height y after jumping from a platform at height y_0.
   *
   * @see game_mechanics.pdf
   */
  static const Time getMinJumpLandingTime(const Length &y_0, const Length &y);
  /**
   * Get the maximum time after which a jumping player may expect to reach
   * the height y after jumping from a platform at height y_0.
   *
   * @see game_mechanics.pdf
   */
  static const Time getMaxJumpLandingTime(const Length &y_0, const Length &y);
  /**
   * Return a random trajectory for a player who wants to go from a platform
   * at height y_0 to a platform at height y.
   *
   * @param randQuantity
   * @param should_jump Whether the player should jump. If the player does not
   * jump, the trajectory is actually not random as there is only one possible
   * trajectory.
   * @param T_init The time when the player should begin to follow this
   * trajectory
   * @param x_0,y_0 The position of the (bottom-left corner of) the player when
   * the trajectory begins
   * @param y The height of the platform the player is expected to reach at the
   * end of the trajectory
   *
   * @see game_mechanics.pdf
   */
  static const JumpTrajectory
  randomTrajectory(const RandomProvider &randQuantity, bool should_jump,
                   const Time &T_init, const Length &x_0, const Length &y_0,
                   const Length &y);

  /**
   *  Get the critical time of the jump, assuming the player glides for t_0.
   * The critical time is the time when the vertical speed is null and
   * Player::getY() is minimum.
   */
  static const Time getCriticalJumpTime(const Time &t_0);

  /**
   *  Get the critical time of the jump.
   * The critical time is the time when the vertical speed is null and
   * Player::getY() is minimum.
   *
   * This method is sensible to be called only when the player should jump,
   * but this condition is not checked.
   */
  const Time getCriticalTime() const;
  /**
   * Get the expected gliding time of the player.
   *
   * This method is sensible to be called only when the player should jump,
   * but this condition is not checked.
   */
  const Time &getExpectedGlidingTime() const;
  /**
   * Get the time after which the player is expected to land on the next
   * platform.
   */
  const Time &getExpectedLandingTime() const;
  /**
   * Indicate whether the player should jump to reach this trajectory.
   */
  bool shouldJump() const;

  /**
   * Get the Y position of the platform the player should land on.
   */
  const Length getTargetY() const;
  /**
   * Get the initial position of (the feet of) the player.
   */
  const Length getInitialY() const;

  /**
   * Get the initial position of the left edge of the player.
   */
  const Length getInitialX() const;

  /**
   * Return the time (since the beginning of the game) when the player
   * should begin to follow this trajectory. It is often written T_init.
   *
   * @see game_mechanics.pdf
   */
  const Time getBeginningOfEvent() const;

  /**
   * Get the Y position of (the feet of) a player at time t,
   * assuming the player follows this trajectory.
   *
   * @param t The time elapsed since the beginning of the trajectory.
   * t should be between 0 and getExpectedLandingTime(), but this
   * condition is not checked.
   */
  const Length getY(const Time &t) const;
  /**
   * The same as getY(const Time&), but with the
   * built-in double number type.
   */
  const double getY(double t) const;

  /**
   * Get the X position of the (left edge of the) player at time t,
   * assuming the player follows this trajectory.
   *
   * @param t The time elapsed since the beginning of the trajectory.
   * t should be between 0 and getExpectedLandingTime(), but this
   * condition is not checked.
   */
  const Length getX(const ISpeedModel &speedModel, const Time &t) const;
  /**
   * The same as getX(const ISpeedModel&, const Time&), but with the
   * built-in double number type.
   */
  const long double getX(const ISpeedModel &speedModel, double t) const;

  void setExpectedLandingTime(const Time &T_p);
};

#endif