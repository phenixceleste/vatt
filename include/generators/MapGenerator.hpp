#ifndef _MAP_GENERATOR_HPP_
#define _MAP_GENERATOR_HPP_

#include "generators/CoinGenerator.hpp"
#include "generators/PlatformGenerator.hpp"

class GameManager;

class MapGenerator {

private:
  PlatformGenerator m_platform_generator;
  CoinGenerator m_coin_generator;

public:
  MapGenerator(GameManager &gameManager);
  void generateMap();
};

#include "GameManager.hpp"

#endif