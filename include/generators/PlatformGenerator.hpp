// -*- lsst-c++ -*-

#ifndef _PLATFORM_GENERATOR_HPP_
#define _PLATFORM_GENERATOR_HPP_

#include "calc/PolynomCalc.hpp"
#include "calc/Quantity.hpp"
#include "generators/JumpTrajectory.hpp"
#include "objects/Platform.hpp"

/**
 * This class provides methods to generate new
 * platforms sequentially, while ensuring that the
 * player can always go from a platform to the next one.
 *
 * @see Read game_mechanics.pdf
 * for explanation of the method and the physics of the game.
 */
class PlatformGenerator {

private:
  /// The maximum Y (minimum height) of the player.
  static const Length PLAYER_MAX_Y;
  /// The minimum Y (maximum height) of the player.
  static const Length PLAYER_MIN_Y;

  /**
   * Fix possible collisions with the side of the platforms due to the
   * player's being approximatively considered as a single material point in the
   * physical model.
   *
   * This method optionally changes T_p (the expected landing time)
   * and Y_p (the height of the new platform) **inplace**.
   * The caller should make sure to reflect the changes.
   */
  void fixCollisions(JumpTrajectory &jumpTrajectory) const;

  GameManager &m_board;

  /**
   * Return a pseudo-random quantity between min and max, with physical
   * dimension Length^L * Time^T, using the GameManager's random generator.
   */
  template <int L, int T>
  const Quantity<L, T> randQuantity(const Quantity<L, T> &min,
                                    const Quantity<L, T> &max);

public:
  PlatformGenerator(GameManager &board) : m_board(board){};

  /**
   * This method should generate one platform (depending on the
   * position of the current right-most platform) and add it to the game.
   *
   * Some additional comments may be provided in the implementation.
   * @see game_mechanics.pdf
   */
  void generatePlatform();
};

#endif