#ifndef _UTIL_HPP_
#define _UTIL_HPP_

#include <string>
#include <vector>

/**
 * Return the concatenation of the stringVect's strings, with a '\n' between
 * each element.
 */
std::string linefeedFold(std::vector<std::string> stringVect);

#endif