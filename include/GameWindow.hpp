// -*- lsst-c++ -*-

#ifndef _GAME_WINDOW_HPP_
#define _GAME_WINDOW_HPP_

#include <SFML/Graphics.hpp>
#include <functional>
#include <vector>

/**
 * A RenderWindow that can be attached to the GameManager to
 * display the game.
 */
class GameWindow : public sf::RenderWindow {

private:
  bool m_fullscreen = true;
  inline static const std::string TITLE = "VATT";
  sf::ContextSettings m_settings;

public:
  GameWindow();
  void toggleFullScreen();
};

#endif
