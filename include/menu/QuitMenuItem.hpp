// -*- lsst-c++ -*-

#ifndef _QUIT_ITEM_HPP_
#define _QUIT_ITEM_HPP_

#include "menu/IMenuItem.hpp"

/**
 * Menu item "Quit game"
 *
 */
class QuitMenuItem : public IMenuItem {

protected:
  /**
   * Quit the game
   * @return true
   */
  virtual bool onClick() override;

public:
  QuitMenuItem(GameManager &game, const sf::Font &font);
};

#endif