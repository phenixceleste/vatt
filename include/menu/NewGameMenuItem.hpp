// -*- lsst-c++ -*-

#ifndef _NEW_GAME_ITEM_HPP_
#define _NEW_GAME_ITEM_HPP_

#include "menu/IMenuItem.hpp"

/**
 * Menu item "New game"
 */
class NewGameMenuItem : public IMenuItem {

protected:
  /**
   * Reset and start the game.
   *
   * @return true
   */
  virtual bool onClick() override;

public:
  NewGameMenuItem(GameManager &game, const sf::Font &font);
};

#endif