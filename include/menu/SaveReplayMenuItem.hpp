// -*- lsst-c++ -*-

#ifndef _SAVE_REPLAY_MENU_ITEM_HPP_
#define _SAVE_REPLAY_MENU_ITEM_HPP_

#include "menu/IMenuItem.hpp"

/**
 * Menu item "Save Replay"
 */
class SaveReplayMenuItem : public IMenuItem {

protected:
  /**
   * Save the replay.
   *
   * @return true
   */
  virtual bool onClick() override;

public:
  SaveReplayMenuItem(GameManager &game, const sf::Font &font);

  /**
   * Reset the item.
   * Overriden because the item must be active only if a game is completed.
   *
   * @see IMenuItem::reset
   */
  virtual void reset() override;
};

#endif