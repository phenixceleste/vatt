// -*- lsst-c++ -*-

#ifndef _CONTINUE_GAME_ITEM_HPP_
#define _CONTINUE_GAME_ITEM_HPP_

#include "menu/IMenuItem.hpp"

/**
 * Menu item "Continue game"
 */
class ContinueGameMenuItem : public IMenuItem {

protected:
  /**
   * Resume the game.
   * @return true
   */
  virtual bool onClick() override;

public:
  ContinueGameMenuItem(GameManager &game, const sf::Font &font);
  /**
   * Reset the item.
   * Overriden because the item must be active only if a game has been started.
   *
   * @see IMenuItem::reset
   */
  virtual void reset() override;
};

#endif