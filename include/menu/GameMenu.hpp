// -*- lsst-c++ -*-

#ifndef _GAME_MENU_HPP_
#define _GAME_MENU_HPP_

#include "menu/ContinueGameMenuItem.hpp"
#include "menu/IMenu.hpp"
#include "menu/NewGameMenuItem.hpp"
#include "menu/QuitMenuItem.hpp"
#include "menu/SaveReplayMenuItem.hpp"
#include "menu/WatchReplayMenuItem.hpp"

/**
 * The default game menu.
 */
class GameMenu : public IMenu {

public:
  /**
   * @brief Construct a new Game Menu object
   *
   * @param game The game the menu belongs to
   * @param font The font to use to display the items
   */
  GameMenu(GameManager &game, const sf::Font &font);
};

#endif