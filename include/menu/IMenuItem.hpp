// -*- lsst-c++ -*-

#ifndef _IMENU_ITEM_HPP_
#define _IMENU_ITEM_HPP_

#include "components/Component.hpp"
#include <SFML/Graphics.hpp>
#include <string>

class GameManager;

/**
 * Base class for menu items.
 * This class must be inherited.
 */
class IMenuItem : public Component {

private:
  /**
   * A wrapper of sf::Text to hold the text of the menu.
   */
  class ItemText : sf::Text {
    friend class IMenuItem;

  private:
    ItemText(const std::string &text);
  };

  bool m_selected = false;
  ItemText m_text;
  /// The default foreground color of the item
  sf::Color m_color = sf::Color::White;
  /// The foreground color of the item when it is selected
  sf::Color m_selected_color = sf::Color::Yellow;
  /// The foreground color of the item when it is inactive
  sf::Color m_inactive_color = sf::Color(150, 150, 150);

protected:
  GameManager &m_game;
  IMenuItem(GameManager &game, const std::string &text);
  IMenuItem(GameManager &game, const std::string &text, const sf::Font &font);

  /**
   * This method is invoked when the item is clicked.
   * Invoked only when the item is active.
   * This method must be overriden.
   *
   * @return true if the click event has been handled,
   *  false otherwise.
   */
  virtual bool onClick();

public:
  void setFont(const sf::Font &font);
  const sf::Font *getFont() const;
  void setText(const std::string &text);
  const std::string getText() const;

  /**
   * Draw the item in the specified clip rectangle of the window.
   * @param window The window to draw in
   * @param clipRect The enclosing rectangle assigned to this item.
   * The rectangle is not guaranteed to have the same size as that
   * returned by getSize() method.
   * The implementation should only draw in this rectangle, but this constraint
   * is not enforced.
   */
  void draw(sf::RenderWindow &window, const sf::FloatRect &clipRect);
  /**
   * Get the preferred size of the item.
   * This size should be computed based on the size necessary to draw the text
   * of the menu item.
   */
  const sf::Vector2f getSize() const;

  /**
   * Notify the item that it has been selected.
   * If the item is active, it should change its color.
   * If the item is not active, the effect is unspecified.
   */
  void selected();
  /**
   * Notify the item that it has been unselected.
   * The item should change its color.
   */
  void unselected();
  /**
   * Notify the item that it has been clicked.
   *
   * @return true if the click event has been handled,
   *  false if the item is not active or the click event
   * cannot be handled
   */
  bool raiseOnClick();
  /**
   * Reset the item. The item is unselected.
   * The base method make the item active.
   *
   * This method can be overriden by subclasses that need
   * specific initialization. Overriding methods should call the
   * super implementation.
   */
  virtual void reset();
  virtual void setActive(bool active) override;
};

#endif