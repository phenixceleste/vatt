// -*- lsst-c++ -*-

#ifndef _IMENU_HPP_
#define _IMENU_HPP_

#include "IMenuItem.hpp"
#include "objects/GameObject.hpp"

/**
 * Base class for menus that display a list of selectable/clickable items.
 * The current implementation requires that at least one item should be active.
 * This class must be inherited.
 */
class IMenu : GameObject {

  typedef std::vector<std::shared_ptr<Component>>::size_type size_type;

private:
  int m_selected_index = 0;
  GameManager &m_manager;
  /**
   * Set the selected item.
   * This method does not check whether the selected item is active.
   *
   * @param selection The position of the selected item.
   * Must be a valid position.
   */
  void setSelection(size_type selection);
  /**
   * Select the next active item.
   * If the last item is already selected, the first item is selected instead.
   */
  void incrSelection();
  /**
   * Select the previous active item.
   * If the first item is already selected, the last item is selected instead.
   */
  void decrSelection();

protected:
  /**
   * Add an item to the menu.
   * The subclasses should not keep a direct reference to the item, nor to the
   * shared_ptr given as parameter; this would cause double frees when
   * the object is destroyed. They should store another shared_ptr to the item
   * instead, if needed.
   */
  void addItem(const std::shared_ptr<IMenuItem> &item);
  /**
   * Get the item at the specified position. The position must be valid.
   */
  IMenuItem *getItem(size_type index) const;

  IMenu(GameManager &manager);

public:
  /**
   * Draw the menu in the window.
   */
  void draw(sf::RenderWindow &window);
  /**
   * Reset the menu and its items.
   * The first active item is selected.
   */
  void reset();
  /**
   * Notify the menu that a key has been pressed.
   */
  void notifyKeyPressed(const sf::Keyboard::Key &key);
};

#endif