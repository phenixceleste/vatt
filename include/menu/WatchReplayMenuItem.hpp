// -*- lsst-c++ -*-

#ifndef _WATCH_REPLAY_MENU_ITEM_HPP_
#define _WATCH_REPLAY_MENU_ITEM_HPP_

#include "menu/IMenuItem.hpp"
#include <filesystem>
#include <fstream>

/**
 * Menu item "WATCH REPLAY"
 */
class WatchReplayMenuItem : public IMenuItem {

protected:
  /**
   * Watch the replay.
   *
   * @return true
   */
  virtual bool onClick() override;

public:
  WatchReplayMenuItem(GameManager &game, const sf::Font &font);

  /**
   * Reset the item.
   * Overriden because the item must be active only if a replay file exists.
   *
   * @see IMenuItem::reset
   */
  virtual void reset() override;
};

#endif