// -*- lsst-c++ -*-

#ifndef _ACTION_HPP_
#define _ACTION_HPP_

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>

#include "util.hpp"

class Action {
public:
  bool m_is_jump;  // 1 -> jump -- 0 -> slide
  bool m_is_press; // 1 -> press -- 0 -> release
  double m_time;

  Action(bool jump, bool press, double time);

  /**
   * Concatenates "0"/"1" for the booleans and the string representation of the
   * file.
   */
  std::string toString();

  /**
   * Takes a string representation of an action to get the corresponding Action.
   */
  static Action fromString(std::string s);

  /**
   * Saves the replay in a file called "save/replay.save".
   * Writes the seed and then the actions.
   *
   */
  static void saveReplayList(std::vector<Action> all_actions,
                             unsigned int seed);

  /**
   * Read the file called "save/replay.save".
   * Writes the seed into the pointeur p_seed.
   * Returns the vector of all the actions.
   *
   */
  static std::vector<Action> loadReplay(unsigned int *seed);
};

#endif