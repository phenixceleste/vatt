// -*- lsst-c++ -*-

#ifndef _COLLECTIBLE_HPP_
#define _COLLECTIBLE_HPP_

#include "objects/CircleEntity.hpp"

/**
 * An entity that can be collected by the user.
 * This class should be inherited.
 * The subclass should also inherit GameObject for consistency reasons.
 */
class Collectible {

private:
  bool m_collected = false;

public:
  Collectible();
  /**
   * Check whether the entity has been collected.
   */
  bool isCollected() const;
  /**
   * Mark the entity has being collected.
   */
  void markCollected();
};

#endif
