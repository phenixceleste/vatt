// -*- lsst-c++ -*-

#ifndef _COMPONENT_HPP_
#define _COMPONENT_HPP_

/**
 * Base class for game components. *
 * A component provides additional functionality to a game object.
 *
 * This class must be inherited.
 */
class Component {

public:
  /**
   * Indicate whether the component should be active.   *
   * The component can alter its behaviour depending on this value,
   * and the parent game object should check it before calling
   * methods that requires this component to be activated.
   */
  bool isActive();
  /**
   * Make the component become active or inactive, depending on the
   * value of the parameter.
   *
   * This method should be overriden by the components that need to
   * listen to activation or deactivation events.
   *
   * @see Component::isActive
   */
  virtual void setActive(bool active);

private:
  bool m_is_active;
};

#endif
