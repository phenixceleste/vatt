// -*- lsst-c++ -*-

#ifndef _SPEED_MODEL_HPP_
#define _SPEED_MODEL_HPP_

#include "calc/Quantity.hpp"
#include "components/Component.hpp"

/**
 * The base class for horizontal speed models.
 *
 * This class must be inherited.
 */
class ISpeedModel : public Component {

public:
  /**
   * Get the speed of the player at time t, where t is the time elapsed
   * since the beginning of the game.
   */
  virtual const Speed getSpeed(const Time &t) const = 0;
  /**
   * Get the X position of the player at time t + T_init,
   * assuming the player was at X = x_0 at time T_init.
   *
   * The result is expressed in the global Cartesian coordinate system.
   */
  virtual const Length getX(const Length &x_0, const Time &T_init,
                            const Time &t) const = 0;
  /**
   * Get the X position of the player at time t,
   * where t is the time elapsed since the beginning of the game.
   *
   * The result is expressed in the global Cartesian coordinate system.
   */
  virtual const Length getX(const Time &t) const = 0;
  /**
   * Get the time when the player has run on a distance 'x'
   */
  virtual const Time getTime(const Length &x) const = 0;
};

/**
 * The model for the horizontal speed defined in game_mechanics.pdf.
 * The speed is in the form of: v_x(iota) = V(iota/k + 1)^a
 */
class HorizontalSpeedModel : public ISpeedModel {
private:
  static const Time T_ref;
  Speed m_v_0;
  double m_k;
  double m_alpha;

  const Scalar getScalarToRaise(const Time &t) const;
  const Length getXCoeff() const;

public:
  /**
   * @param v_0,k,alpha The parameters of the model, as defined in
   * game_mechanics.pdf.
   */
  HorizontalSpeedModel(const Speed &v_0, double k, double alpha);
  virtual const Speed getSpeed(const Time &t) const override;
  virtual const Length getX(const Length &x_0, const Time &T_init,
                            const Time &t) const override;
  virtual const Length getX(const Time &t) const override;
  virtual const Time getTime(const Length &x) const override;
};

#endif