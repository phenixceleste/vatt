// -*- lsst-c++ -*-

#ifndef _GRAPHIC_COMPONENT_HPP
#define _GRAPHIC_COMPONENT_HPP

#include "Vector2d.hpp"
#include "components/Component.hpp"
#include <SFML/Graphics.hpp>
#include <filesystem>
#include <map>
#include <string>

class RectangleEntity;

#ifdef _WIN32
typedef std::string filesystem_path;
#else
typedef std::filesystem::path filesystem_path;
#endif

/**
 * This component provides methods for game objects
 * that use textures to display.
 *
 * @note setActive(bool) has no effect on this component,
 */
class GraphicComponent : public Component {

public:
  /**
   * Construct a new GraphicComponent object
   */
  GraphicComponent();

  /**
   * Notify the GraphicComponent that a frame has happened.
   * If an animation is started, the frame counter is updated
   * and the active texture is updated if appropriate.
   * If the texture is frozen, this method has no effect.
   */
  void nextFrame();
  /**
   * Get the texture currently active in this component.
   *
   * @throws If no texture has been set (either with setTexture or animate).
   */
  const sf::Texture &getTexture();
  /**
   * Make the given frozen (non-animated) texture become the active
   * texture of this component.
   * The texture is automatically loaded from the file if needed.
   *
   * @param path The path of the image file that contains the texture.
   */
  void setTexture(const filesystem_path &path);
  /**
   * Start a repeating animation between the given textures.
   * The textures will be activated one after the other, according to
   * their initial order in 'paths'.
   * The next texture is activated when the previous one has remained activated
   * for 'framesPerTexture' frames.
   * The animation will be effective only if nextFrame is called appropriately.
   *
   * @param paths The paths of the image files that contain the texture, in the
   * same order as the animation's expected order.
   * The textures are automatically loaded from the files if needed.
   * If 'paths' contains only one element, 'framesPerTexture' is ignored and
   * this method is equivalent to setTexture.
   * If 'paths' is empty, this GraphicComponent will no longer have an active
   * texture, and a new active texture must be set before getTexture may be
   * called again.
   * @param framesPerTexture The number of calls to nextFrame to wait before
   * activating the next texture.
   * @throws If framesPerTexture is 0.
   * @see nextFrame()
   * @see setAnimationFrame(unsigned int)
   */
  void animate(const std::vector<filesystem_path> &paths,
               unsigned int framesPerTexture);
  /**
   * Activate the specified frame.
   * If id is out of bounds, it is replaced by the remainder of its division by
   * the number of textures in the animation. If the texture is frozen, this
   * method has no effect.
   */
  void setAnimationTexture(unsigned int id);

  /**
   * Load the texture contained in the given file.
   * If the texture has already been loaded, this method
   * has no effect.
   * The texture will stay loaded until the GraphicComponent
   * is destroyed.
   *
   * @param path The path of the image file that contains the texture.
   */
  void load(const filesystem_path &path);
  /**
   * Load the textures contained in all the files in the vector.
   * The textures that have already been loaded are ignored.
   *
   * @param paths The paths of the image files that contain the texture.
   * @see GraphicComponent::load(const filesystem_path &)
   */
  void load(const std::vector<filesystem_path> &paths);

private:
  std::map<filesystem_path, sf::Texture> m_textures;

  /// Number of frames/tick to wait before changing the texture
  /// in the case of an animation.
  unsigned int m_frame_per_texture = 1;
  /// The paths of the active textures.
  /// Contains one element for a frozen texture
  /// and multiple elements for an animation.
  std::vector<filesystem_path> m_paths;
  /// The position of the current texture of the animation.
  /// Will be 0 if the texture is not animated
  unsigned int m_current_animation_frame = 0;
  /// Number of frames since the last texture change.
  unsigned int m_frame_count = 0;

  /**
   * Indicate whether the texture contained in the given
   * file has already been loaded.
   *
   * @param path The path of the image file that contains the texture.
   */
  bool isLoaded(const filesystem_path &path) const;
};

#endif
