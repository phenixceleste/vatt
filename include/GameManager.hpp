// -*- lsst-c++ -*-
#ifndef _GAMEMANAGER_HPP_
#define _GAMEMANAGER_HPP_

#include "Action.hpp"
#include "GameWindow.hpp"
#include "OptionalRef.hpp"
#include "Session.hpp"
#include "TimeManager.hpp"
#include "components/SpeedModel.hpp"
#include "generators/MapGenerator.hpp"
#include "menu/GameMenu.hpp"
#include "objects/Coin.hpp"
#include <SFML/Graphics.hpp>
#include <functional>
#include <random>
#include <vector>

class Player;
class Platform;

/**
 * A wrapper for the game.
 *
 * Note on coordinates:
 * The size of the game is independent from the size of the display screen.
 * All physical objects should use coordinates in game unit.
 * The conversion to screen unit should be done at display time only.
 *
 * The coordinate system is in rectilinear (horizontal) translation at speed
 * getGameSpeed() in relation to any coordinate system that is fixed in the
 * terrestrial reference frame.
 */
class GameManager {
public:
  typedef std::function<void(GameManager &)> OnGameQuitListener;
  typedef std::function<void(GameManager &)> OnGameOverListener;
  typedef std::function<void(GameManager &)> OnTickListener;
  typedef std::function<void(GameManager &)> OnManagerLaunchedListener;

  /**
   * Screen-independent width of the game.
   */
  static const int TOTAL_HORIZONTAL_DIV = 1800;
  /**
   * Screen-independent height of the game.
   */
  static const int TOTAL_VERTICAL_DIV = 1000;

  static double constexpr PLAYER_WIDTH = TOTAL_HORIZONTAL_DIV / 30.0;
  static double constexpr PLAYER_HEIGHT = TOTAL_VERTICAL_DIV / 11.45;

  /**
   * Unit: vertical div per second^2
   */
  static double constexpr GRAVITY_ACCELERATION = TOTAL_VERTICAL_DIV / 1.7;

private:
  /**
   * The tick rate. This affects both the update rate and the frame rate.
   * The actual frame rate may be lower depending on the host computer.
   */
  const static unsigned int TICK_RATE = 60;
  constexpr static double TICK_INTERVAL = 1.0 / TICK_RATE;

  TimePoint m_last_tick;

  /**
   * This class contains the state of the current game.
   */
  class GameState {
    friend class GameManager;

  private:
    GameState(GameManager &manager, unsigned int seed = 0,
              unsigned int num_player = 1);

    unsigned int m_seed;
    double m_score = 0;
    bool m_alive = false;
    bool m_running = false;
    bool m_replaying = false;
    bool m_tick_pending = false;
#ifdef _TEST
    bool m_player_running = true;
#endif
    long double m_time = 0;

    std::vector<Player> m_players;
    std::vector<Platform> m_platforms;
    std::vector<Coin> m_coins;
    std::vector<Action> m_all_actions;

    std::default_random_engine m_random_generator;
    std::uniform_real_distribution<> m_real_distrib;

    GameState &operator=(GameState &&state);
  };

  bool m_launched = false;
  bool m_need_redraw = true;
#ifdef _TEST
  bool m_fast_mode = false;
#endif

  Session &m_session;
  HorizontalSpeedModel m_speed_model;
  MapGenerator m_map_generator;
  GameState m_state;
  GameMenu m_menu;
  TimeManager m_time_manager;

  OptionalRef<GameWindow &> m_window;
  bool m_close_on_quit = true;
  bool m_input_enabled = true;

  std::vector<OnGameQuitListener> m_game_quit_listeners;
  std::vector<OnGameOverListener> m_game_over_listeners;
  std::vector<OnTickListener> m_tick_listeners;
  std::vector<OnManagerLaunchedListener> m_manager_launched_listener;

  static sf::Font font;

  /**
   * Update the game and its object based on the elapsed time (since last
   * udpate).
   *
   * @param elapsed In seconds.
   * @return false If the player has lost (true otherwise)
   */
  bool update(double elapsed);

  /**
   * Do routine stuff if appropriate.
   * The routine stuff includes:
   * - updating the game, if a game is running
   * - rendering the game, if a window is attached
   */
  void tick();

  /**
   * Called after the game has been updated.
   * Responsible for calling listeners.
   */
  void onTick();

  /**
   * This method is responsible to run tick repeatedly until
   * the game is quitted.
   */
  void manager_loop();

  /**
   * Notify the game that a key has been pressed.
   * The event is sent either to the game or the menu, depending
   * on the state of the game.
   */
  void notifyKeyPressed(const sf::Keyboard::Key &);
  /**
   * Notify the game that a key has been released.
   * The event is sent either to the game or the menu, depending
   * on the state of the game.
   */
  void notifyKeyReleased(const sf::Keyboard::Key &);

  /**
   * Notify a SFML event to the game.
   * The way the event is handled depends on the type of the event and
   * the state of the game.
   * Unsupported or unrecognized events are ignored.
   *
   * @see notifyKeyPressed
   * @see notifyKeyReleased
   */
  void notifyEvent(const sf::Event &);

  /// Called when the game quits. Responsible for calling listeners.
  void onGameQuit();
  /// Called when the game stops. Responsible for calling listeners.
  void onGameStop();

  /**
   * Draw the game in the window. Should NOT draw the backround.
   * Calling this method requires that a window should be attached.
   */
  void drawAlive();

  /**
   * Draw the menu in the attached window (when the game is not running).
   * Should NOT draw the backround.
   * Calling this method requires that a window should be attached.
   */
  void drawMenu();

  /**
   * Draw the entire game in the attached window.
   * Calling this method requires that a window should be attached.
   */
  void draw();

  /**
   * Draw the game in the attached window, if any, and process input events,
   * if input is enabled.
   * If no window is attached, this method has no effect.
   */
  void render();

  /**
   * Indicate whether the manager should redraw the game.
   */
  bool shouldRedraw();

  /**
   * Indicates whether new platforms should be generated.
   */
  bool needsNewPlatform();

  /**
   * Generate the map if needed.
   */
  void generateMap();

public:
  /**
   * Construct a new Game Manager object
   *
   * @param session The session is the object used to create a save feature.
   * @param seed The seed to initialize the random generator. Leave 0 to use
   * a default value based on the current time.
   * @param num_player The number of players in the game. Must be 1 for now.
   */
  GameManager(Session &session, unsigned int seed = 0,
              unsigned int num_player = 1);
  ~GameManager();

  /**
   * Load the assets (e.g. the font).
   * This static method should be called once and for all at the beginning of
   * the program.
   * If a GameManager is created before this method is called, the behaviour is
   * unspecified.
   */
  static void loadAssets();

  /**
   * Returns the current horizontal speed of the game, in
   * horizontal-div-per-second.
   */
  double getGameSpeed() const;

  /**
   * Convert an abscissa x from the global Cartesian
   * coordinate system to the game coordinate system.
   *
   * Note: the game coordinate system is in rectilinear (horizontal) translation
   * at speed v_x(iota) = getGameSpeed() in relation to the global (fixed)
   * coordinate system.
   *
   * @see game_mechanics.pdf
   */
  double getLocalX(const Length &x) const;

  const ISpeedModel &getGameSpeedModel() const;

#ifdef _TEST
  /**
   * Make the player stop running (i.e. force getGameSpeed() to return 0).
   * This operation is irreversible for the game in progress.
   * Used only by tests.
   */
  void stopRunning();

  /**
   * Make the GameManager not wait between ticks.
   * This does not affect the virtual in-game time between ticks.
   */
  void runInFastMode();
#endif
  /**
   * Get the score of the player. The score should be a function of the time
   * elapsed since the beginning of the game.
   */
  double getScore() const;
  /**
   * Get the time elapsed since the game has started.
   *
   * @note The time is not guaranteed to be the same as the time,
   * although the score is obviously likely to increase over time.
   */
  double getElapsedTime() const;
  /**
   * Get the player by id.
   *
   * @param id The id of the player. Should be 0 for now.
   * @throws If no player is found with this id.
   */
  Player &getPlayer(int id);
  /**
   * Get the session used to save the progress of the user.
   */
  Session &getSession();

  /**
   * @brief Returns true if the player is lying on a platform,
   * false if they are falling or in the air.
   * @param platform If the player is on a
   * platform, the OptionalRef will contain the platform the player is lying on.
   * Else, the OptionalRef is left with its initial value.
   */
  bool isOnAPlatform(const Player &player,
                     OptionalRef<const Platform &> &platform);
  /**
   * @brief Returns true if the player is colliding any of the platforms.
   *
   * @param platform If the player is in collision with a
   * platform, the OptionalRef will contain the collided platform.
   * Else, the OptionalRef is left with its initial value.
   */
  bool isCollidingPlatform(const Player &player,
                           OptionalRef<const Platform &> &platform);

  /**
   * Return a pseudo-random double value between min and max.
   */
  double randDouble(double min, double max);

  /**
   * Return the last (typically the right-most) platform.
   */
  const Platform &getLastPlatform();
  /**
   * Get the trajectory that a player is expected to follow when going
   * from the penultimate platform to the last one.
   * getLastPlatform must have been called at least once before this method
   * may be called.
   */
  const JumpTrajectory &getLastJumpTrajectory();

  /**
   * Add a platform to the game.
   * @param platform The platform to add
   */
  void addPlatform(const Platform &platform,
                   const JumpTrajectory &jumpTrajectory);
  /**
   * Add a coin to the game.
   */
  void addCoin(const Coin &coin);

  /**
   * Launch the game manager. This call BLOCKS until the manager is shut down.
   * This method should be called in the thread that owns the attached window
   * (preferably the main thread).
   *
   * @throws If the game manager is already launched.
   */
  void launchManager();

  /**
   * Stop the game and shut the manager down. The thread that initially launched
   * the game manager is unblocked. If the game manager is not launched, this
   * method has no effect. The game manager may be launched again after it has
   * been shut down.
   */
  void quitGame() noexcept;
  /**
   * Start the game. If the game has already been started, this method has no
   * effect.
   * The manager must be launched before the game is started.
   *
   * @param replaying if we start a replay
   *
   * @throws If the manager is not launched.
   */
  void startGame(bool replaying = false);
  /**
   * Stop the game. It is then necessary to call resetGame() before
   * starting the game again.
   * If the game is not running, this method only resets the menu.
   */
  void stopGame() noexcept;
  /**
   * Pause the game without stopping it.
   * If the game is already paused, or is not started at all, this method only
   * resets the menu.
   */
  void pauseGame() noexcept;
  /**
   * Resume the game. If the game is not paused, or is not started at all, this
   * method has no effect.
   */
  void resumeGame() noexcept;
  /**
   * Pause the game if it is running, and resume the game if it is paused.
   * If the game is not started, this method does NOT start it (but may reset
   * the menu).
   */
  void pauseOrResumeGame() noexcept;

  /**
   * Resets the game. If the game is running, it is automatically stopped.
   * When this method returns, the game is ready to be started with startGame().
   * It is not necessary to call this method before starting the game for the
   * first time.
   *
   * @param seed The seed to initialize the random generator. Leave 0 to use
   * a default value based on the current time.
   * @param num_player The number of players in the game. Must be 1 for now.
   */
  void resetGame(unsigned int seed = 0, unsigned int num_player = 1);

  /**
   * Saves the Actions into a file that can be read later.
   */
  void saveReplay();

  /**
   * Executes a given Action when replaying
   */

  void executeAction(Action action);

  /**
   * Loads the replay from the dedicated file and simulate it.
   *
   */
  void watchReplay();

  /**
   * Is the GameManager in replay mode
   */
  bool isInReplayMode();

  /**
   * Indicate whether the player is alive.
   * It is equivalent to asking whether the game is started.
   */
  bool isAlive();

  /**
   * Indicate whether the game is running.
   * The game is running if it is started, not paused and the
   * manager is launched.
   */
  bool isRunning();

  /**
   * Return the time manager associated to this game manager.
   */
  TimeManager &getTimeManager();
  /**
   * Set the time manager of this game manager.
   * The time manager cannot be changed when a game is started.
   * Note: using a time manager with a high speed does not make the
   * computer compute faster. This will cause the tick pace to increase
   * and lead to inaccuracies in the computations of the moves.
   *
   * @throws If a game is started.
   */
  void setTimeManager(const TimeManager &timeManager);

  /**
   * Register a listener to be invoked when the game is quitted.
   */
  void addGameQuitListener(const OnGameQuitListener &listener);
  /**
   * Register a listener to be invoked when the game is stopped
   * (either because the player has lost or the game is stopped through the
   * menu).
   */
  void addGameOverListener(const OnGameOverListener &listener);

  /**
   * Register a listener to be invoked when the manager is launched.
   */
  void addManagerLaunchedListener(const OnManagerLaunchedListener &listener);

  /**
   * Register a listener to be invoked every tick.
   * The listeners are called only if a game is running.
   *
   * Note: the listener should execute fast because no more tick
   * can be run before all the listeners have returned.
   */
  void addTickListener(const OnTickListener &listener);

  /**
   * Attach a window to the game manager.
   * The attached window will be used to display the game and to send
   * input events to the game.
   * If a window is already attached, it is detached and replaced with
   * the new one.
   */
  void attachWindow(GameWindow &window);
  /**
   * Detach the currently attached window. If no window is attached,
   * this method has no effect.
   * Detaching a window does NOT close it.
   */
  void detachWindow();
  /**
   * Indicate whether the game accepts inputs from the attached window.
   * This is true by default.
   */
  bool isInputEnabled() const;
  /// Allow the game to receive future inputs from the attached window.
  void enableInput();
  /**
   * Tell the game not to process future inputs from the attached window,
   * until enableInput is called.
   */
  void disableInput();
  /**
   * Indicate whether the attached window (if any) should be closed
   * when the game is quitted.
   * Defaults to true.
   */
  bool willCloseWindowOnQuit() const;
  /**
   * Set whether the attached window (if any) should be closed
   * when the game is quitted.
   */
  void setCloseWindowOnQuit(bool closeOnQuit);

  /**
   * Tell the GameManager that it should redraw the game at next tick.
   */
  void invalidateDisplay();
};

#include "objects/Platform.hpp"
#include "objects/Player.hpp"

#endif
