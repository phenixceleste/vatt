#ifndef _TIME_MANAGER_HPP_
#define _TIME_MANAGER_HPP_

#include "OptionalRef.hpp"
#include <chrono>

class TimeManager;

/**
 * An opaque time point used to measure durations
 * without disclosing the actual representation of the time.
 */
class TimePoint {
  friend class TimeManager;

private:
  OptionalRef<TimeManager &> m_parent;
  std::chrono::system_clock::time_point m_time;
  TimePoint(OptionalRef<TimeManager &> &parent,
            const std::chrono::system_clock::time_point &time);

public:
  /**
   * Create an unitialized time point.
   * This method allows to declare variables of type TimePoint
   * while deferring their initialization.
   * The unitialized time point should not be used.
   */
  TimePoint();
  /**
   * Return the integral number of seconds elapsed between the current time
   * point and the clock's epoch (typically Thu Jan 1 00:00:00 1970).
   */
  unsigned int time_since_epoch() const;

  /**
   * Return the time elapsed (in seconds) between the two time points.
   * The two time points must have been initialized by the same
   * time manager.
   * The time elapsed takes into account the speed of the time manager.
   */
  double operator-(const TimePoint &rhs) const;
  /**
   * Return a new time point that is 'time' seconds ahead of
   * the current time point.
   */
  const TimePoint operator+(double time) const;
  /**
   * Adds in-place the given duration (in seconds) to this
   * time point.
   */
  TimePoint &operator+=(double time);
};

/**
 * This class provides a method to get a TimePoint corresponding to
 * the current system point.
 * It encapsulates the manipulation of the built-in C++ time API.
 *
 */
class TimeManager {

  friend class TimePoint;

private:
  double m_scale;

public:
  /**
   * Construct a new Time Manager object
   * @param speed The virtual speed of the time.
   * A consumer using this object will be under the
   * impression that the time goes 'speed' times as fast as the real clock's
   * time. A value of 1.0 corresponds to a normal speed.
   */
  TimeManager(double speed = 1.0);
  /**
   * Return the current time.
   */
  const TimePoint getCurrentTime();
  double operator-(const TimePoint &rhs) const;
  const TimePoint operator+(double time) const;
  TimePoint &operator+=(double time);
};

#endif