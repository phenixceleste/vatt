// -*- lsst-c++ -*-

#ifndef __SESSION_HPP_
#define __SESSION_HPP_

#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "util.hpp"

/**
 * The session stores persistent data about the progress of
 * the player, such as its best score or the number of
 * coins they have collected so far.
 * The session is responsible for loading and saving the data
 * on disk.
 */
class Session {
private:
  std::string m_filename = "";

  long m_bestScore = 0;
  long m_coins = 0;

  /**
   * Create a new session attached to the given file.
   * The path is relative to the "save" directory.
   * This does NOT load existing progress from this file.
   * See Session::fromFile instead.
   */
  Session(std::string filename);
  /**
   * Create a new session from the data contained
   * in the vector. See Session::fromFile.
   */
  Session(std::vector<std::string> &stringVect);

public:
  /**
   * Create a new session from the specified file.
   * If the file exists, the progress it contains is
   * loaded in the session.
   * If the file does not exist, it is created.
   *
   * @param filename The path of the file, relative to the
   * "save" directory.
   */
  static Session fromFile(std::string filename);
  /**
   * Create a new blank session attached to no file.
   */
  static Session temp();

  /**
   * Compare the score with the current best score, and
   * update the best score if the new score is higher.
   */
  void updateBestScore(long score);

  /**
   * Increase the collected coins counter by amount.
   */
  void addCoin(long amount);

  /**
   * Return the player's best score.
   */
  long getBestScore();
  /**
   * Return the number of coins that the player has collected so far.
   */
  long getCoins();

  /**
   * Save the session to the attached file.
   * If this session has no file attached, this method
   * has no effect.
   */
  void save();
};

#endif