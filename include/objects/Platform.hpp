// -*- lsst-c++ -*-
#ifndef PLATFORM_INCLUDED
#define PLATFORM_INCLUDED

#include "generators/JumpTrajectory.hpp"
#include "objects/Coin.hpp"
#include "objects/PlaneEntity.hpp"
#include "objects/RectangleEntity.hpp"
#include <optional>

class GameManager;

/**
 * A platform is the entity the player will have
 * to jump in not to fall out of the world.
 */
class Platform : public PlaneEntity {

private:
  std::optional<JumpTrajectory> m_jump_trajectory;
  Length m_actual_X_p;

protected:
  virtual void specializeConvexShape(sf::ConvexShape &shape) override;

public:
  /**
   * Construct a new platform.
   *
   * @param actual_X_p The position of the platform in the global (fixed)
   * coordinate system.
   * @param x,y,width Arguments are self-explanatory. All values must be given
   * in game unit.
   */
  Platform(const Length &actual_X_p, double x, double y, double width);

  /**
   * Set the jump trajectory that a player is expected to follow
   * when going from this platform to the next one.
   */
  void setJumpTrajectory(const JumpTrajectory &jumpTrajectory);
  /**
   * Indicate whether a jump trajectory has been associated with
   * this platform. A platform should always have an associated jump
   * trajectory, unless it is the right-most platform of the game.
   */
  bool hasJumpTrajectory() const;
  /**
   * Get the trajectory that a player is expected to follow when
   * going from this platform to the next one.
   *
   * @throw If there is no such trajectory available (this should
   * happen only if the platform is the right-most platform of the game).
   * @see hasJumpTrajectory
   */
  const JumpTrajectory &getJumpTrajectory() const;
  /**
   * Get the X position of the platform in the global (fixed) coordinate system.
   */
  const Length &getActualX() const;
};

#endif
