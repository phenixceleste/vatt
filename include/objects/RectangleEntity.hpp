#ifndef _RECTANGLE_ENTITY_HPP_
#define _RECTANGLE_ENTITY_HPP_

#include "objects/PlaneEntity.hpp"
#include "objects/ShapedEntity.hpp"
#include <SFML/Graphics.hpp>

class PlaneEntity;

/**
 * A rectangle entity is a physical game object whose
 * shape is an axis-aligned rectangle.
 *
 * This class must be inherited.
 */
class RectangleEntity : public ShapedEntity<sf::RectangleShape> {

private:
  double m_x;
  double m_y;
  double m_width;
  double m_height;

protected:
  /**
   * Construct a new Rectangle Entity object
   *
   * @param x,y,width,height Self-explanatory. All values must be given in game
   * unit.
   */
  RectangleEntity(double x, double y, double width, double height);

  /**
   * Specializes the rectangle shape such that it effectively contains
   * the specific appearance of the object.
   *
   * This method must be overriden.
   *
   * @param shape The rectangle shape to specialize, whose size and
   * position already match those of the current entity.
   * The shape must be changed in-place. Its position and size should not be
   * touched.
   */
  virtual void specializeRectangleShape(sf::RectangleShape &shape) = 0;

protected:
  /**
   * Get the rectangle shape that should be drawn in 'window' to
   * display the object.
   */
  const sf::RectangleShape
  getShape(const sf::RenderWindow &window) override final;

public:
  /**
   * Move the entity by the given offset (in game unit).
   */
  void move(double x, double y);

  /**
   * Indicate whether this entity is colliding with the specified
   * RectangleEntity.
   */
  bool isColliding(const RectangleEntity &other) const;

  /**
   * Get the X (horizontal) position of the entity, in game unit.
   */
  double getX() const;
  /**
   * Set the X (horizontal) position of the entity, in game unit.
   */
  void setX(double x);
  /**
   * Get the Y (vertical) position of the entity, in game unit.
   */
  double getY() const;
  /**
   * Get the Y (vertical) position of the entity, in game unit.
   */
  void setY(double y);
  /**
   * Get the width of the entity, in game unit.
   */
  double getWidth() const;
  /**
   * Set the width of the entity, in game unit.
   */
  void setWidth(double width);
  /**
   * Get the height of the entity, in game unit.
   */
  double getHeight() const;
  /**
   * Set the height of the entity, in game unit.
   */
  void setHeight(double height);
  /**
   * Convenience method to get the right position of the entity, in game unit.
   * Equivalent to @code{.cpp} getX() + getWidth() @endcode
   */
  double getRight() const;
  /**
   * Convenience method to get the right position of the entity, in game unit.
   * Equivalent to @code{.cpp} getY() + getHeight() @endcode
   */
  double getBottom() const;
  /**
   * Put this entity just above the given plane entity, such that
   * they do not collide when this method returns.
   */
  void putJustAbove(const PlaneEntity &other);
  /**
   * Put this entity just below the given plane entity, such that
   * they do not collide when this method returns.
   */
  void putJustBelow(const PlaneEntity &other);
  /**
   * Indicate whether this entity is just above the given plane entity.
   */
  bool isJustAbove(const PlaneEntity &other) const;
};

#ifdef _DEBUG
inline std::ostream &operator<<(std::ostream &os, const RectangleEntity &rhs) {
  os << "Entity{" << std::to_string(rhs.getX()) << ";"
     << std::to_string(rhs.getY()) << ";" << std::to_string(rhs.getWidth())
     << ";" << std::to_string(rhs.getHeight()) << "}";
  return os;
}
#endif

#endif
