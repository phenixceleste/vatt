// -*- lsst-c++ -*-

#ifndef _DISPLAYABLE_ENTITY_HPP_
#define _DISPLAYABLE_ENTITY_HPP_

#include "Vector2d.hpp"
#include "objects/GameObject.hpp"

/**
 * The base class for all GameObjects that can be displayed.
 * This class must be inherited.
 */
class DisplayableEntity : public GameObject {

protected:
  DisplayableEntity();

public:
  /**
   * Returns a vector to scale the entity.
   * The vector contains the factors to convert from game units to screen units.
   */
  const sf::Vector2d getScaleVector(const sf::RenderWindow &window) const;

  /**
   * Draw this entity in 'window'.
   * This method must be overriden.
   */
  virtual void draw(sf::RenderWindow &window) = 0;
};

#endif