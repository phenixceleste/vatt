// -*- lsst-c++ -*-

#ifndef _CIRCLE_ENTITY_HPP_
#define _CIRCLE_ENTITY_HPP_

#include "objects/RectangleEntity.hpp"
#include "objects/ShapedEntity.hpp"
#include <SFML/Graphics.hpp>

class RectangleEntity;

/**
 * A circle entity is a physical game object whose
 * shape is a circle.
 *
 * This class must be inherited.
 */
class CircleEntity : public ShapedEntity<sf::CircleShape> {

private:
  double m_x;
  double m_y;
  double m_radius;
  size_t m_point_count;

protected:
  /**
   * Construct a new circle entity.
   * @param x,y,radius Self-explanatory. (In game unit.)
   * @param point_count The number of points to use to draw the circle.
   */
  CircleEntity(double x, double y, double radius, size_t point_count)
      : m_x(x), m_y(y), m_radius(radius), m_point_count(point_count) {}

  /**
   * Specializes the circle shape such that it effectively contains
   * the specific appearance of the object.
   *
   * This method must be overriden.
   *
   * @param shape The circle shape to specialize, whose the position and the
   * radius already match those of the current entity. The shape must be changed
   * in-place. Its position and radius should not be touched.
   */
  virtual void specializeCircleShape(sf::CircleShape &shape) = 0;

protected:
  /**
   * Get the circle shape that should be drawn in 'window' to
   * display the object.
   */
  virtual const sf::CircleShape
  getShape(const sf::RenderWindow &window) override final;

public:
  /**
   * Indicate whether this entity is colliding with the specified
   * RectangleEntity.
   */
  bool isColliding(const RectangleEntity &other) const;
  /**
   * Move the entity by the specified offset.
   *
   * @param x,y The offset, in game unit.
   */
  void move(double x, double y);

  /**
   * Get the X (horizontal) position of the entity, in game unit.
   */
  double getX() const;
  /**
   * Set the X (horizontal) position of the entity, in game unit.
   */
  void setX(double x);
  /**
   * Get the Y (vertical) position of the entity, in game unit.
   */
  double getY() const;
  /**
   * Set the Y (vertical) position of the entity, in game unit.
   */
  void setY(double y);
  /**
   * Get the radius of the object, in game unit.
   */
  double getRadius() const;
  /**
   * Set the radius of the object, in game unit.
   */
  void setRadius(double radius);
  /**
   * Convenience method to get the right position of the entity, in game unit.
   * Equivalent to @code{.cpp} getX() + getRadius() @endcode
   */
  double getRight() const;
};

#ifdef _DEBUG
inline std::ostream &operator<<(std::ostream &os, const CircleEntity &rhs) {
  os << "Entity{" << std::to_string(rhs.getX()) << ";"
     << std::to_string(rhs.getY()) << ";" << std::to_string(rhs.getRadius())
     << "}";
  return os;
}
#endif

#endif
