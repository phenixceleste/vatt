// -*- lsst-c++ -*-

#ifndef PLAYER_INCLUDED
#define PLAYER_INCLUDED

#include "Collectible.hpp"
#include "GameManager.hpp"
#include "components/GraphicComponent.hpp"
#include "objects/Coin.hpp"
#include <stdexcept>

class Player : public RectangleEntity {
private:
  const double m_base_height;
  int m_player_id = -1;
  double m_gliding = 0;
  bool m_rolling = false;
  double m_speed_y;
  bool m_bent_down = false;
  bool m_is_on_a_platform = true;

  GameManager &m_board;

  static std::vector<filesystem_path> animation_frames;
  GraphicComponent m_graphic_component;

  /**
   * @brief Tries to move the player vertically to new_y, unless a platform
   * hinders the move.
   *
   * @param new_y The vertical coordinate the player must move to, in game unit.
   * Should be computed with m_speed_y
   */
  void fall(double new_y);
  /**
   * Update the texture depending on the state of the player.
   * This method must be called only when the state of the player
   * changes in a way that affects their appearance.
   */
  void updateTexture();

protected:
  virtual void specializeRectangleShape(sf::RectangleShape &shape) override;

public:
  /**
   * Construct a new Player object. Arguments are self-explanatory.
   * All values must be given in game unit.
   */
  Player(double x, double y, double width, double height, GameManager &board);

  /**
   * The duration (in number of ticks) of each frame of the animation.
   */
  const static unsigned int ANIMATION_FRAMES_DURATION = 5;

  /**
   * Initial vertical speed when initiating a jump (in vertical div per
   * second).
   */
  static double constexpr BASE_JUMP_SPEED = GameManager::TOTAL_VERTICAL_DIV / 3;
  /**
   * Glide power, in vertical div per second square.
   * This is the initial acceleration due to gliding.
   * @see game_mechanics.pdf for more explanation
   */
  static double constexpr BASE_GLIDE_POWER = GameManager::GRAVITY_ACCELERATION;
  /**
   * Maximum gliding duration (in seconds).
   */
  static double constexpr MAX_GLIDE_DURATION = 1.5;

  /**
   * Factor by which the height of the player is to be multiplied when they
   * are bent.
   */
  static double constexpr BENT_HEIGHT_FACTOR = 0.75;
  /**
   * Get the id of the player.
   */
  int getPlayerId() const;

  /**
   * Make the player jump, provided they are on a platform.
   * If the player is not on a platform, or they are bending down, this method
   * has no effect.
   *
   * If the player actually jumps, they will then be gliding
   * until one of the following condition is reached:
   * - the player lands on a platform,
   * - stopJump is called, or
   * - the player has been gliding for MAX_GLIDE_DURATION before
   * the previous conditions were reached.
   */
  void startJump() noexcept;

  /**

   * Makes the player stop gliding.
   * If the player was not gliding, this method has no effect.

   */
  void stopJump() noexcept;

  /**
   * Make the player bend down. This is not yet useful.
   * The player will be bent down until stopRoll is called.
   *
   * The player cannot bend down if they are in the air.
   * If the player is gliding, this method has no effect.
   * If the player is no longer gliding but is still in the air,
   * the effect of this method is delayed until the player hits the
   * ground again.
   */
  void startRoll();

  /**
   * Make the player pick themselves up.
   * If the player was not bent down, this method has no effect.
   *
   * The player cannot pick themselves up if they are in the air.
   * Therefore, if the player is not on a platform, the effect of this method
   * is delayed until the player hits the ground again.
   */
  void stopRoll();

  void collect(Coin &coin);

  /**
   * Updates the player depending on the elapsed time (since last update).
   *
   * @note For further information about the physics of the game
   * please read game_mechanics.pdf.
   *
   * @param elapsed In seconds.
   * @return false If the player has fallen out the game (true otherwise)
   */

  bool tick(double elapsed);

  /**
   * Load the assets
   * This static method should be called once and for all at the beginning of
   * the program.
   * If a player is created before this method is called, the behaviour is
   * unspecified.
   */
  static void loadAssets();
};

#endif
