#ifndef _SHAPED_ENTITY_HPP_
#define _SHAPED_ENTITY_HPP_

#include "objects/DisplayableEntity.hpp"
#include <SFML/Graphics.hpp>

/**
 * The base class for DisplayableEntities that
 * use a sf::Shape to draw themselves.
 *
 * This class must be inherited.
 *
 * @tparam T Must be a subclass of sf::Shape.
 */
template <class T> class ShapedEntity : public DisplayableEntity {

protected:
  ShapedEntity() {
    static_assert(std::is_base_of<sf::Shape, T>::value,
                  "T must be derived from sf::Shape.");
  }
  /**
   * Get the shape that will be used to display the entity.
   * This method must be overriden.
   */
  virtual const T getShape(const sf::RenderWindow &window) = 0;

public:
  /**
   * Draw this entity in 'window'.
   */
  virtual void draw(sf::RenderWindow &window) override final {
    window.draw(getShape(window));
  }
};

#endif