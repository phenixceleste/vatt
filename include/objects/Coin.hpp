// -*- lsst-c++ -*-

#ifndef _COIN_HPP_
#define _COIN_HPP_

#include "Collectible.hpp"

/**
 * A coin.
 * Coins are dispersed in the map and must be collected by the player.
 */
class Coin : public CircleEntity, public Collectible {
private:
  static size_t constexpr COIN_NUMBER_OF_POINTS = 20;

protected:
  virtual void specializeCircleShape(sf::CircleShape &shape) override;

public:
  static double constexpr COIN_RADIUS = 10.0;

  Coin(double x, double y);
};

#endif
