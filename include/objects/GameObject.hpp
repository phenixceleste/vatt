// -*- lsst-c++ -*-

#ifndef _GAME_OBJECT_HPP_
#define _GAME_OBJECT_HPP_

#include "components/Component.hpp"
#include <memory>
#include <vector>

/**
 * Base class for game objects.
 * This class must be inherited.
 */
class GameObject {

public:
  /**
   * Get the list of components of this game object.
   *
   * @note When appropriate, the subclasses should also provide
   * methods to access specific components.
   */
  const std::vector<std::shared_ptr<Component>> getComponentList() const;
  /**
   * Convenience method to get the component count of this game object.
   * Equivalent to @code{.cpp} getComponentList().size() @endcode.
   */
  const typename std::vector<std::shared_ptr<Component>>::size_type
  getComponentCount() const;

protected:
  /**
   * Add a component to this object.
   *
   * @warning Subclasses should not keep a direct reference to the component,
   * nor to the shared_ptr given as parameter; this would cause double frees
   * when the object is destroyed. They should store another shared_ptr to the
   * component instead, if needed.
   */
  void addComponent(std::shared_ptr<Component> new_component);

private:
  std::vector<std::shared_ptr<Component>> m_components;
};

#endif