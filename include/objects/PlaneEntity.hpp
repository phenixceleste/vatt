#ifndef _PLANE_ENTITY_HPP_
#define _PLANE_ENTITY_HPP_

#include "objects/RectangleEntity.hpp"
#include "objects/ShapedEntity.hpp"
#include <SFML/Graphics.hpp>

class RectangleEntity;

/**
 * A plane entity is a physical game object whose
 * shape is an axis-aligned horizontal line.
 *
 * This class must be inherited.
 */
class PlaneEntity : public ShapedEntity<sf::ConvexShape> {

private:
  double m_x;
  double m_y;
  double m_width;
  size_t m_point_count;

protected:
  /**
   * Construct a new plane entity. Arguments are self-explanatory.
   * All values should be given in game unit.
   */
  PlaneEntity(double x, double y, double width)
      : m_x(x), m_y(y), m_width(width) {}

  /**
   * Specializes the convex shape such that it effectively contains
   * the specific appearance of the object.
   *
   * This method must be overriden.
   *
   * @param shape The convex shape to specialize, whose width and
   * position already match those of the current entity.
   * The shape must be changed in-place. Its position, its width and its being
   * an axis-aligned horizontal line should not be touched.
   */
  virtual void specializeConvexShape(sf::ConvexShape &shape) = 0;

protected:
  /**
   * Get the convex shape that should be drawn in 'window' to
   * display the object.
   */
  virtual const sf::ConvexShape
  getShape(const sf::RenderWindow &window) override final;

public:
  /**
   * Indicate whether this entity is colliding with the specified
   * RectangleEntity.
   */
  bool isColliding(const RectangleEntity &other) const;
  /**
   * Move the entity by the specified offset.
   *
   * @param x,y The offset, in game unit.
   */
  void move(double x, double y);

  /**
   * Get the X (horizontal) position of the entity, in game unit.
   */
  double getX() const;
  /**
   * Set the X (horizontal) position of the entity, in game unit.
   */
  void setX(double x);
  /**
   * Get the Y (vertical) position of the entity, in game unit.
   */
  double getY() const;
  /**
   * Set the Y (vertical) position of the entity, in game unit.
   */
  void setY(double y);
  /**
   * Get the width of the object, in game unit.
   */
  double getWidth() const;
  /**
   * Set the width of the object, in game unit.
   */
  void setWidth(double width);
  /**
   * Convenience method to get the right position of the object, in game unit.
   * Equivalent to @code{.cpp} getX() + getWidth() @endcode.
   */
  double getRight() const;
};

#ifdef _DEBUG
inline std::ostream &operator<<(std::ostream &os, const PlaneEntity &rhs) {
  os << "Entity{" << std::to_string(rhs.getX()) << ";"
     << std::to_string(rhs.getY()) << ";" << std::to_string(rhs.getWidth())
     << "}";
  return os;
}
#endif

#endif
