// -*- lsst-c++ -*-

#ifndef _TEST_JUMP_HPP
#define _TEST_JUMP_HPP

#include "GameManager.hpp"
#include "Test.hpp"
#include <tuple>

/**
 * Test to check that the actual jump trajectory of the player
 * is consistent with the trajectory predicted by the physical model.
 *
 * This test simulates two jumps and compares the result with the predicted
 * results:
 * - a jump when the player does not glide
 * - a jump when the player glides as long as possible (i.e. for T_max)
 *
 * @see game_mechanics.pdf
 */
class TestJump : public Test<> {

public:
  TestJump();

protected:
  virtual bool onTick(GameManager &game) override;
  virtual bool onBeforeRun(GameManager &game) override;
  virtual bool wasSuccessfull(GameManager &game) override;

private:
  double DURATION_THRESHOLD;
  double HEIGHT_THRESHOLD;

  double EXP_H_MIN;
  double EXP_H_MAX;

  double T_1;
  double T_2;

  double SMALL_DURATION;
  double LONG_DURATION;

  static const int STEP_SMALL_JUMP = 0;
  static const int STEP_SMALL_JUMP_IN_PROGRESS = 1;
  static const int STEP_LONG_JUMP = 2;
  static const int STEP_LONG_JUMP_IN_PROGRESS = 3;

  bool m_successful = true;
  int m_step = STEP_SMALL_JUMP;
  double m_jump_start = 0;
  double m_max_reach_time = 0;
  double m_init_y = 0;
  double m_jump_height = 0;

  /**
   * Make the player jump and resets the internal logs of any previous jump.
   */
  void startJump(GameManager &game);
  /**
   * Update the internal log of the jump in progress.
   * @return true if the jump is complete, false otherwise.
   */
  bool updateJumpLog(GameManager &game);
  /**
   * Print the internal logs about the jump.
   *
   * @return true if the actual trajectory of the jump was close enough to the
   * expected trajectory, false otherwise.
   */
  bool printJumpDetails(GameManager &game, double expectedDuration,
                        double expectedJumpHeight, double expectedCriticalTime);
};

#endif