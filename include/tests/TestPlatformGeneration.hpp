// -*- lsst-c++ -*-

#ifndef _TEST_PLAT_GEN_HPP
#define _TEST_PLAT_GEN_HPP

#include "GameManager.hpp"
#include "Test.hpp"

/**
 * A test to check that the platform are generated properly,
 * and the physical model is consistent with the actual results.
 *
 * This test makes play an AI until it loses or the time limit is reached
 * (-1 means no time limit).
 * The test succeeds if the AI does not lose.
 * If no time limit is set, the test cannot succeed and must be stopped
 * manually (with Ctrl + C).
 *
 * @see game_mechanics.pdf
 */
class TestPlatformGeneration : public Test<int> {

public:
  TestPlatformGeneration();

protected:
  virtual bool onTick(GameManager &game, const int &time_limit) override;
  virtual bool wasSuccessfull(GameManager &game,
                              const int &time_limit) override;

private:
  bool m_successful = false;
  std::optional<JumpTrajectory> m_expected_trajectory;

  /**
   * Print verbose information when the player dies (this should not happen).
   */
  void detailFailure(GameManager &game) const;
  /**
   * When the jump of the fall is complete, check that the player has hit the
   * platform at the expected location, and show a debug message otherwise.
   *
   * @param game
   * @param player
   * @param platform The platform the player has hit.
   */
  // void checkTrajectory(GameManager &game, Player &player, const Platform
  // &platform) const;
};

#endif