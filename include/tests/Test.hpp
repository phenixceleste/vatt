// -*- lsst-c++ -*-

#ifndef _TEST_HPP_
#define _TEST_HPP_

#include "GameWindow.hpp"
#include "Session.hpp"
#include <unistd.h>

#ifndef _TEST
#error                                                                         \
    "Should not compile tests without defining _TEST! Make sure your command is correct."
#endif

/**
 * Base class for tests.
 * This class must be inherited.
 *
 * @tparam Args The arguments that the test requires to run,
 * in addition to the game window and the game manager.
 */
template <typename... Args> class Test {

private:
  /**
   * Print a debug message when the test has failed.
   */
  void testFailed() {
    std::cout << "\033[1;31mTEST FAILED: \033[0m" << m_name << std::endl;
  }
  /**
   * Print a debug message when the test is successful.
   */
  void testSucceeded() {
    std::cout << "\033[1;32mTEST SUCCEEDED: \033[0m" << m_name << std::endl;
  }

protected:
  /**
   * This method is called after every tick.
   *
   * @param game  The game manager that is used for the test
   * @param arguments The additional arguments that are used by the test.
   *
   * @return true to continue the test, false to stop it (no matter whether
   * the test has succeeded or not).
   */
  virtual bool onTick(GameManager &game, const Args &...arguments) = 0;
  /**
   * Initialize the test before it is run. The base method only starts the game.
   * Subclasses may override this method.
   * If this method is overriden, the overriding method is responsible for
   * starting the game if needed.
   *
   * @param game The game manager that will be used for the test. The manager is
   * already launched.
   * @param arguments The additional arguments that will be used by the test.
   * @return true if the test is ready to be run, false to cancel the
   * execution of the test. If this method returns false, it should also
   * print a message to explain why the test cannot be run.
   */
  virtual bool onBeforeRun(GameManager &game, const Args &...arguments) {
    game.startGame();
    return true;
  }
  /**
   * Indicate whether the test completed successfully.
   * If onBeforeRun has returned false, this method should return false as well.
   *
   * @param game  The game manager that is used for the test
   * @param arguments The additional arguments that are used by the test.
   */
  virtual bool wasSuccessfull(GameManager &game, const Args &...arguments) = 0;
  /**
   * Indicate whether the window should accept user input during the test.
   * The default implementation returns false.
   * This method may be overriden to change this behaviour.
   */
  virtual bool allowInput() const { return false; }

  /// The user-friendly name of the test. Used only to print debug messages.
  std::string m_name;
  Test(const std::string &name) : m_name(name) {}

public:
  /**
   * @brief Run the test in the given window, with the given arguments (if any).
   *
   * @param window
   * @param fast true if the test should be run in fast mode
   * @return true if the test is successful, false otherwise.
   */
  bool runTest(GameWindow &window, bool fast, const Args &...arguments) {

    if (!window.isOpen()) {
      std::cout << "Giving up running the test " << m_name
                << " because the window was closed." << std::endl;
      return false;
    }

    Session session = Session::temp();
    GameManager game(session);
    if (fast) {
      game.runInFastMode();
    }

    if (!allowInput()) {
      game.disableInput();
    }

    game.attachWindow(window);

    game.addManagerLaunchedListener([this, arguments...](GameManager &game) {
      if (!this->onBeforeRun(game, arguments...)) {
        game.quitGame();
      }
    });
    game.addTickListener([this, arguments...](GameManager &game) {
      if (!this->onTick(game, arguments...)) {
        game.quitGame();
      }
    });

    game.launchManager();
    game.detachWindow();

    if (wasSuccessfull(game, arguments...)) {
      testSucceeded();
      return true;
    } else {
      testFailed();
      return false;
    }
  }
};

#endif