// -*- lsst-c++ -*-

#ifndef _OPTIONAL_REF_HPP_
#define _OPTIONAL_REF_HPP_

#include <optional>

/**
 * An alternative to std::optional for reference
 * data types.
 * It's mainly a class that encapsulates the checks for null pointer
 * and pointer's dereferences.
 *
 * @tparam T Should be a reference to another type: T2&.
 */
template <typename T> class OptionalRef;

template <typename T> class OptionalRef<T &> {

private:
  T *__data;

public:
  /**
   * Construct an optional with no value inside.
   */
  OptionalRef() : __data(NULL) {}
  /**
   * Construct an optional containing the given value.
   */
  OptionalRef(T &data) : __data(&data) {}

  /**
   * Replace the value contained in this OptionalRef with the given value.
   * The value previously pointed at is untouched.
   */
  void inplace(T &data) { __data = &data; }
  /**
   * Clear the value contained in this OptionalRef.
   * When this method returns, this optional is empty.
   * The value previously pointed at is untouched.
   */
  void clear() { __data = NULL; }

  /**
   * Access the contained value.
   *
   * @throws std::bad_optional_access if this object contains
   * no value.
   */
  const T &operator*() const {
    if (*this) {
      return *__data;
    }
    throw std::bad_optional_access();
  }

  /**
   * Returns a reference to the contained value.
   *
   * @throws std::bad_optional_access if this object contains
   * no value.
   */
  const T *operator->() const {
    if (*this) {
      return __data;
    }
    throw std::bad_optional_access();
  }

  /**
   * Access the contained value.
   *
   * @throws std::bad_optional_access if this object contains
   * no value.
   */
  T &operator*() {
    if (*this) {
      return *__data;
    }
    throw std::bad_optional_access();
  }

  /**
   * Returns a reference to the contained value.
   *
   * @throws std::bad_optional_access if this object contains
   * no value.
   */
  T *operator->() {
    if (*this) {
      return __data;
    }
    throw std::bad_optional_access();
  }

  /**
   * Check whether this optional contains a value.
   */
  operator bool() const { return __data != NULL; }
};

// To help debug wrong type parameters.
class T_is_not_a_reference_type {};

template <typename T>
class OptionalRef : std::enable_if<std::is_lvalue_reference<T>::value,
                                   T_is_not_a_reference_type>::type {};

#endif