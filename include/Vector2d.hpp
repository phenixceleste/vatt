#ifndef _VECTOR2D_HPP_
#define _VECTOR2D_HPP_

#include <SFML/Graphics.hpp>

namespace sf {
typedef sf::Vector2<double> Vector2d;
}

#endif