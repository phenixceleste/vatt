#ifndef _TRANSFORM_HPP_
#define _TRANSFORM_HPP_

#include <tuple>

class Transform {

public:
  Transform();

  std::tuple<float, float> getPos();
  void setPos(float new_x_pos, float new_y_pos);

  float getRot();
  void setRot(float new_rot);

private:
  float m_x_pos;
  float m_y_pos;
  float m_rotation;
};

#endif
