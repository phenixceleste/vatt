// -*- lsst-c++ -*-

#ifndef _POLYNOMCALC_HPP_
#error "Please include 'PolynomCalc.hpp' instead."
#endif
#include "calc/PolynomCalc.hpp" //For the IDE only

/**
 * A polynomial of degree (at most) 4 that provides
 * a method to retrieve its roots.
 *
 * @tparam Lx,Tx,Ly,Ty @see Polynomial @see WithRoots
 */
template <int Lx, int Tx, int Ly, int Ty>
class Polynomial4 : public WithRoots<4, Lx, Tx, Ly, Ty> {

private:
  using parent = WithRoots<4, Lx, Tx, Ly, Ty>;

  /**
   * Get the greatest real item from the list.
   *
   * @tparam L,T The dimensions of the quantities that the list stores.
   */
  template <int L, int T>
  const Quantity<L, T>
  greatest_real(const std::vector<Quantity<L, T>> &lst) const {
    auto t = lst.end();
    for (auto i = lst.begin(); i != lst.end(); ++i) {
      if (i->isReal() && (t == lst.end() || *i > *t)) {
        t = i;
      }
    }
    return *t;
  }

public:
  using parent::WithRoots; // Inherit constructors
  Polynomial4(const parent &p) : parent(p) {}

  /// Coefficient A (of degree 4)
  const Quantity<Ly - 4 * Lx, Ty - 4 * Tx> &coeffA() const {
    return parent::template coeff<4>();
  }
  /// Coefficient B (of degree 3)
  const Quantity<Ly - 3 * Lx, Ty - 3 * Tx> &coeffB() const {
    return parent::template coeff<3>();
  }
  /// Coefficient C (of degree 2)
  const Quantity<Ly - 2 * Lx, Ty - 2 * Tx> &coeffC() const {
    return parent::template coeff<2>();
  }
  /// Coefficient D (of degree 1)
  const Quantity<Ly - Lx, Ty - Tx> &coeffD() const {
    return parent::template coeff<1>();
  }
  /// Coefficient E (of degree 0)
  const Quantity<Ly, Ty> &coeffE() const { return parent::template coeff<0>(); }

  std::vector<Quantity<Lx, Tx>> roots() const override {

    if (coeffA().isNull()) {
      return Polynomial3<Lx, Tx, Ly, Ty>(parent::template degree_cast<3>())
          .roots();
    }

    const auto asquare = coeffA().square();
    const Quantity<2 * Lx, 2 *Tx> p =
        -(3 * coeffB().square()) / (8 * asquare) + coeffC() / coeffA();
    const Quantity<3 * Lx, 3 *Tx> q = (coeffB() / (2 * coeffA())).cube() -
                                      coeffB() * coeffC() / (2 * asquare) +
                                      coeffD() / coeffA();
    const Quantity<4 * Lx, 4 *Tx> r =
        -3 * (coeffB() / (4 * coeffA())).template pow<4>() +
        coeffC() * (coeffB() / 4).square() / coeffA().cube() -
        coeffB() * coeffD() / (4 * asquare) + coeffE() / coeffA();

    if (q.isNull()) {

      std::vector<Quantity<2 * Lx, 2 *Tx>> sol2 =
          Polynomial2<2 * Lx, 2 * Tx, 4 * Lx, 4 * Tx>(Scalar(1), p, r).roots();

      std::vector<Quantity<Lx, Tx>> solutions;

      for (int i = 0; i < 2; i++) {
        const Quantity<Lx, Tx> root = sol2[i].sqrt();
        solutions.push_back(root);
        solutions.push_back(-root);
      }

      return solutions;

    } else {

      std::vector<Quantity<2 * Lx, 2 *Tx>> sol3 =
          Polynomial3<2 * Lx, 2 * Tx, 6 * Lx, 6 * Tx>(Scalar(1), -p, -4 * r,
                                                      4 * p * r - q.square())
              .roots();

      Quantity<2 * Lx, 2 *Tx> u = greatest_real(sol3);

      const Quantity<Lx, Tx> z = q / (2 * (u - p));

      std::vector<Quantity<Lx, Tx>> solutions;

      std::vector<Quantity<Lx, Tx>> sol2 =
          Polynomial2<Lx, Tx, 2 * Lx, 2 * Tx>(Scalar(1), -(u - p).sqrt(),
                                              u / 2 + z * (u - p).sqrt())
              .roots();
      solutions.insert(solutions.begin(), sol2.begin(), sol2.end());

      sol2 = Polynomial2<Lx, Tx, 2 * Lx, 2 * Tx>(Scalar(1), (u - p).sqrt(),
                                                 u / 2 - z * (u - p).sqrt())
                 .roots();
      solutions.insert(solutions.begin() + 2, sol2.begin(), sol2.end());

      for (int i = 0; i < 4; i++) {
        solutions[i] = solutions[i] - coeffB() / (4 * coeffA());
      }

      return solutions;
    }
  }

  const Polynomial4<Lx, Tx, Ly, Ty> operator-() const {
    return Polynomial4<Lx, Tx, Ly, Ty>(parent::operator-());
  }
  const Polynomial4<Lx, Tx, Ly, Ty>
  operator+(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial4<Lx, Tx, Ly, Ty>(parent::operator+(rhs));
  }
  const Polynomial4<Lx, Tx, Ly, Ty>
  operator-(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial4<Lx, Tx, Ly, Ty>(parent::operator-(rhs));
  }
  const Polynomial4<Lx, Tx, Ly, Ty> operator*(const Scalar &rhs) const {
    return Polynomial4<Lx, Tx, Ly, Ty>(parent::operator*(rhs));
  }
  const Polynomial4<Lx, Tx, Ly, Ty> operator/(const Scalar &rhs) const {
    return Polynomial4<Lx, Tx, Ly, Ty>(parent::operator/(rhs));
  }
};

template <int Lx, int Tx, int Ly, int Ty>
const Polynomial4<Lx, Tx, Ly, Ty>
operator*(const Scalar &lhs, const Polynomial4<Lx, Tx, Ly, Ty> &rhs) {
  return Polynomial4(lhs * (Polynomial<4, Lx, Tx, Ly, Ty>)rhs);
}