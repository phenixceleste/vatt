// -*- lsst-c++ -*-

#ifndef _TUPLE_HPP_
#define _TUPLE_HPP_

#include <functional>
#include <tuple>

/**
 * Custom class to hold a tuple.
 *
 * @tparam a The type of the elements that the tuple stores. Empty list is
 * supported.
 */
template <typename... a> class tuple;

template <> class tuple<> : public std::tuple<> {
private:
  using parent = typename std::tuple<>;

public:
  tuple() = default;
  tuple(const std::tuple<> &tpl) : parent(tpl) {}
};

template <typename... a> class tuple : public std::tuple<a...> {
private:
  using parent = typename std::tuple<a...>;

public:
  tuple() = default;
  tuple(const std::tuple<a...> &tpl) : parent(tpl) {}
  tuple(const a &...values) : parent(values...) {}
};

/**
 * Generic utility class to build tuple type recursively.
 * It contains a typedef 'type' that contains the flattened tuple type.
 */
template <typename... a> class flatten {};

/**
 * Build an empty tuple type
 */
template <> class flatten<> {
public:
  using type = tuple<>;
};
/**
 * Build the type tuple<a, b...> from the types a and tuple<b...>.
 *
 * @tparam a The new type to append to the left of the tuple.
 * @tparam b The types of the elements that the base tuple stores.
 */
template <typename a, typename... b> class flatten<a, tuple<b...>> {
public:
  using type = tuple<a, b...>;

  static const type build(const a &head, const std::tuple<b...> &tail) {
    return type(std::tuple_cat(std::make_tuple(head), tail));
  }
};

namespace std {

/**
 * Return the tail of the tuple (i.e. the tuple without its first element).
 *
 * @tparam a The type of the first element stored by the tuple
 * @tparam b The types of the rest of the elements that the tuple stores.
 * @param tpl The tuple to extract the tail from.
 */
template <typename a, typename... b>
tuple<b...> tail(const tuple<a, b...> &tpl) {

  return apply([](auto head, auto... tail) { return std::make_tuple(tail...); },
               tpl);
}
} // namespace std

#endif