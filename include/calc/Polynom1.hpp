// -*- lsst-c++ -*-

#ifndef _POLYNOMCALC_HPP_
#error "Please include 'PolynomCalc.hpp' instead."
#endif
#include "calc/PolynomCalc.hpp" //For the IDE only

/**
 * A polynomial of degree (at most) 1 that provides
 * a method to retrieve its roots.
 *
 * @tparam Lx,Tx,Ly,Ty @see Polynomial @see WithRoots
 */
template <int Lx, int Tx, int Ly, int Ty>
class Polynomial1 : public WithRoots<1, Lx, Tx, Ly, Ty> {

private:
  using parent = WithRoots<1, Lx, Tx, Ly, Ty>;

public:
  using parent::WithRoots; // Inherit constructors
  Polynomial1(const parent &p) : parent(p) {}

  /// Coefficient A (of degree 1)
  const Quantity<Ly - Lx, Ty - Tx> &coeffA() const {
    return parent::template coeff<1>();
  }
  /// Coefficient B (of degree 0)
  const Quantity<Ly, Ty> &coeffB() const { return parent::template coeff<0>(); }

  std::vector<Quantity<Lx, Tx>> roots() const override {
    std::vector<Quantity<Lx, Tx>> solutions;
    if (!coeffA().isNull()) {
      solutions.push_back(-coeffB() / coeffA());
    }
    return solutions;
  }

  const Polynomial1<Lx, Tx, Ly, Ty> operator-() const {
    return Polynomial1<Lx, Tx, Ly, Ty>(parent::operator-());
  }
  const Polynomial1<Lx, Tx, Ly, Ty>
  operator+(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial1<Lx, Tx, Ly, Ty>(parent::operator+(rhs));
  }
  const Polynomial1<Lx, Tx, Ly, Ty>
  operator-(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial1<Lx, Tx, Ly, Ty>(parent::operator-(rhs));
  }
  const Polynomial1<Lx, Tx, Ly, Ty> operator*(const Scalar &rhs) const {
    return Polynomial1<Lx, Tx, Ly, Ty>(parent::operator*(rhs));
  }
  const Polynomial1<Lx, Tx, Ly, Ty> operator/(const Scalar &rhs) const {
    return Polynomial1<Lx, Tx, Ly, Ty>(parent::operator/(rhs));
  }
};

template <int Lx, int Tx, int Ly, int Ty>
const Polynomial1<Lx, Tx, Ly, Ty>
operator*(const Scalar &lhs, const Polynomial1<Lx, Tx, Ly, Ty> &rhs) {
  return Polynomial1(lhs * (Polynomial<1, Lx, Tx, Ly, Ty>)rhs);
}