// -*- lsst-c++ -*-

#ifndef _QUANTITY_HPP_
#define _QUANTITY_HPP_

#include <cmath>
#include <complex.h>
#include <math.h>
#include <stdexcept>

#undef I // Conflicts with SFML (key I)
class Scalar;

/**
 * A physical quantity, with physical dimensions.
 * This class provides compile-time analysis of the homogeneity of a calculus.
 * Multiplying or dividing by a scalar is allowed.
 * Only integral length and time dimensions are implemented.
 *
 * @tparam LengthDim
 * @tparam TimeDim
 */
template <int LengthDim, int TimeDim> class Quantity {
public:
  using dtype = long double;

private:
  std::complex<dtype> m_value;

public:
  template <int, int> friend class Quantity;
  friend class Scalar;

  Quantity() : m_value(0, 0) {}
  explicit Quantity(const std::complex<dtype> &val) : m_value(val) {}
  explicit Quantity(dtype val) : m_value(val, 0) {}
  Quantity(const Quantity<LengthDim, TimeDim> &other)
      : m_value(other.m_value) {}

  /**
   * Get the raw value of this quantity.
   */
  const std::complex<dtype> getValue() const { return m_value; }
  /**
   * Assume that the quantity has a real value that can be represented by
   * the double data type, and return this value.
   * @throw If the quantity has a non-real value.
   */
  double requireRealValue() const { return (double)requireLongRealValue(); }
  /**
   * Assume that the quantity has a real value, and return this value.
   * @throw If the quantity has a non-real value.
   */
  long double requireLongRealValue() const {
    if (m_value.imag() != 0) {
      throw std::logic_error("Quantity: Called requireRealValue but the object "
                             "contains a non-real value!");
    }
    return m_value.real();
  }

  /**
   * Return true if and only if this quantity is null.
   */
  bool isNull() const { return m_value == 0.0l; }
  /**
   * Return true if and only if this quantity is real and positive or null.
   */
  bool isPositive() const { return isReal() && m_value.real() >= 0; }
  // Self-explanatory
  bool isStrictlyPositive() const { return !isNull() && isPositive(); }
  // Self-explanatory
  bool isReal() const { return m_value.imag() == 0; }
  /**
   * Return the raw norm (aka the absolute value) of this quantity.
   * This method discards the dimension.
   */
  dtype norm() const {
    return std::sqrt(std::pow(m_value.real(), 2) + std::pow(m_value.imag(), 2));
  }
  /**
   * Raise this quantity to the power of K and return the result.
   * The result has the appropriate dimension.
   * This instance is untouched.
   * The implementation uses std::pow.
   *
   * @tparam K
   */
  template <int K> const Quantity<K * LengthDim, K * TimeDim> pow() const {
    return Quantity<K * LengthDim, K * TimeDim>(std::pow(m_value, K));
  }
  /**
   * Convenience method to raise the quantity to the power of 2.
   * @see Quantity::pow
   */
  const Quantity<2 * LengthDim, 2 * TimeDim> square() const {
    return this->template pow<2>();
  }
  /**
   * Convenience method to raise the quantity to the power of 3.
   * @see Quantity::pow
   */
  const Quantity<3 * LengthDim, 3 * TimeDim> cube() const {
    return this->template pow<3>();
  }

  /**
   * Compute the K'th root of this quantity and return the result.
   * The result has the appropriate dimension.
   * This instance is untouched.
   *
   * This method is only defined if the result would have integral dimensions.
   *
   * @tparam K If K = 2, the implementation uses std::sqrt.
   * If K = 3, the implementation uses std::cbrt.
   * Otherwise, the implementation uses std::pow.
   */
  template <unsigned int K>
  typename std::enable_if<
      K != 0 && K != 1 && LengthDim % K == 0 && TimeDim % K == 0,
      const Quantity<LengthDim / (int)K, TimeDim / (int)K>>::type
  root() const {

    if (K == 2) {
      return Quantity<LengthDim / (int)K, TimeDim / (int)K>(std::sqrt(m_value));
    } else if (K == 3 && isReal()) {
      return Quantity<LengthDim / (int)K, TimeDim / (int)K>(
          std::cbrt(requireRealValue()));
    } else {
      return Quantity<LengthDim / (int)K, TimeDim / (int)K>(
          std::pow(m_value, 1.0 / K));
    }
  }

  /**
   * Convenience method to compute the square root of this quantity.
   *
   * This method is only defined if the result would have integral dimensions.
   *
   * @tparam K
   * @see Quantity::root
   */
  template <int L = LengthDim / 2, int T = TimeDim / 2>
  typename std::enable_if<LengthDim == 2 * L && TimeDim == 2 * T,
                          const Quantity<L, T>>::type
  sqrt() const {
    return this->root<2>();
  }

  /**
   * Convenience method to compute the cubic root of this quantity.
   *
   * This method is only defined if the result would have integral dimensions.
   *
   * @tparam K
   * @see Quantity::root
   */
  template <int L = LengthDim / 3, int T = TimeDim / 3>
  typename std::enable_if<LengthDim == 3 * L && TimeDim == 3 * T,
                          const Quantity<L, T>>::type
  cbrt() const {
    return this->root<3>();
  }

  const Quantity<LengthDim, TimeDim>
  operator+(const Quantity<LengthDim, TimeDim> &rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value + rhs.m_value);
  }
  const Quantity<LengthDim, TimeDim>
  operator-(const Quantity<LengthDim, TimeDim> &rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value - rhs.m_value);
  }
  const Quantity<LengthDim, TimeDim> operator-() const {
    return Quantity<LengthDim, TimeDim>(-m_value);
  }
  template <int L2, int T2>
  const Quantity<LengthDim + L2, TimeDim + T2>
  operator*(const Quantity<L2, T2> &rhs) const {
    return Quantity<LengthDim + L2, TimeDim + T2>(m_value * rhs.m_value);
  }
  template <int L2, int T2>
  const Quantity<LengthDim - L2, TimeDim - T2>
  operator/(const Quantity<L2, T2> &rhs) const {
    return Quantity<LengthDim - L2, TimeDim - T2>(m_value / rhs.m_value);
  }
  /**
   * Indicates whether this quantity has the same value as the parameter.
   */
  bool operator==(const Quantity<LengthDim, TimeDim> &rhs) const {
    return m_value == rhs.m_value;
  }
  /**
   * Indicates whether this instance is strictly greater than the parameter.
   * If one or both quantities is not real, this function returns false.
   */
  bool operator>(const Quantity<LengthDim, TimeDim> &rhs) const {
    return isReal() && rhs.isReal() &&
           requireRealValue() > rhs.requireRealValue();
  }
  /**
   * Indicates whether this instance is strictly lower than the parameter.
   * If one or both quantities is not real, this function returns false.
   */
  bool operator<(const Quantity<LengthDim, TimeDim> &rhs) const {
    return isReal() && rhs.isReal() &&
           requireRealValue() < rhs.requireRealValue();
  }
  /**
   * Indicates whether this instance is greater than or equal to the parameter.
   * If one or both quantities is not real, this function returns false.
   */
  bool operator>=(const Quantity<LengthDim, TimeDim> &rhs) const {
    return isReal() && rhs.isReal() &&
           requireRealValue() >= rhs.requireRealValue();
  }
  /**
   * Indicates whether this instance is lower than or equal to the parameter.
   * If one or both quantities is not real, this function returns false.
   */
  bool operator<=(const Quantity<LengthDim, TimeDim> &rhs) const {
    return isReal() && rhs.isReal() &&
           requireRealValue() <= rhs.requireRealValue();
  }
  /**
   * Indicates whether this quantity has a different same value from the
   * parameter.
   */
  bool operator!=(const Quantity<LengthDim, TimeDim> &rhs) const {
    return m_value != rhs.m_value;
  }

  const Quantity<LengthDim, TimeDim>
  operator*(const std::complex<dtype> &rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value * rhs);
  }
  const Quantity<LengthDim, TimeDim>
  operator/(const std::complex<dtype> rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value / rhs);
  }

  const Quantity<LengthDim, TimeDim> operator*(dtype rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value * rhs);
  }
  const Quantity<LengthDim, TimeDim> operator/(dtype rhs) const {
    return Quantity<LengthDim, TimeDim>(m_value / rhs);
  }
};

/**
 * Convenience type for scalar quantities.
 * If a scalar value is only intended to multiply a Quantity,
 * the base types dtype and std::complex<dtype> may be used instead.
 */
class Scalar : public Quantity<0, 0> {
public:
  Scalar(int value) : Quantity((dtype)value){};
  Scalar(dtype value) : Quantity(value){};
  Scalar(const std::complex<dtype> value) : Quantity(value){};
  Scalar(const Quantity<0, 0> value) : Quantity(value){};

  /**
   * Compute the cosinus of this scalar value (measured in radians).
   */
  const Scalar cos() const { return Scalar(std::cos(m_value)); }
  /**
   * Compute the principal value of the arc cosine of this scalar value.
   */
  const Scalar acos() const { return Scalar(std::acos(m_value)); }

  /**
   * Raise this scalar to the power of 'power' and return the result.
   * This instance is untouched.
   * The implementation uses std::pow.
   */
  const Scalar pow(dtype power) const {
    return Scalar(std::pow(m_value, power));
  }

  const Scalar operator+(const Scalar &rhs) const {
    return Scalar(Quantity::operator+(rhs));
  }
  const Scalar operator-(const Scalar &rhs) const {
    return Scalar(Quantity::operator-(rhs));
  }

  const Scalar operator+(const dtype &rhs) const {
    return Scalar(m_value + rhs);
  }
  const Scalar operator-(const dtype &rhs) const {
    return Scalar(m_value - rhs);
  }

  const Scalar operator+(const std::complex<dtype> &rhs) const {
    return Scalar(m_value + rhs);
  }
  const Scalar operator-(const std::complex<dtype> &rhs) const {
    return Scalar(m_value - rhs);
  }
};

/**
 * A convenience type for quantities with one dimension of length and no
 * dimension of time.
 */
typedef Quantity<1, 0> Length;
/**
 * A convenience type for quantities with no dimension of length and one
 * dimension of time;
 */
typedef Quantity<0, 1> Time;
/**
 * A convenience type for quantities homogeneous to a speed.
 */
typedef Quantity<1, -1> Speed;
/**
 * A convenience type for quantities homogeneous to a acceleration.
 */
typedef Quantity<1, -2> Acceleration;

template <int L, int T>
const Quantity<L, T>
operator*(const std::complex<typename Quantity<L, T>::dtype> &lhs,
          const Quantity<L, T> &rhs) {
  return Quantity<L, T>(lhs * rhs.getValue());
}
template <int L, int T>
const Quantity<-L, -T>
operator/(const std::complex<typename Quantity<L, T>::dtype> &lhs,
          const Quantity<L, T> &rhs) {
  return Quantity<-L, -T>(lhs / rhs.getValue());
}

template <int L, int T>
const Quantity<L, T> operator*(typename Quantity<L, T>::dtype lhs,
                               const Quantity<L, T> &rhs) {
  return Quantity<L, T>(lhs * rhs.getValue());
}
template <int L, int T>
const Quantity<-L, -T> operator/(typename Quantity<L, T>::dtype lhs,
                                 const Quantity<L, T> &rhs)

{
  return Quantity<-L, -T>(lhs / rhs.getValue());
}

#ifdef _DEBUG
template <int L, int T>
std::ostream &operator<<(std::ostream &os, const Quantity<L, T> &rhs) {
  const std::complex<typename Quantity<L, T>::dtype> value = rhs.getValue();
  os << "(" << value.real() << " + " << value.imag() << "i)";
  return os;
}
#endif

#endif