// -*- lsst-c++ -*-

#ifndef _POLYNOMCALC_HPP_
#error "Please include 'PolynomCalc.hpp' instead."
#endif
#include "calc/PolynomCalc.hpp" //For the IDE only

/**
 * A polynomial of degree (at most) 3 that provides
 * a method to retrieve its roots.
 *
 * @tparam Lx,Tx,Ly,Ty @see Polynomial @see WithRoots
 */
template <int Lx, int Tx, int Ly, int Ty>
class Polynomial3 : public WithRoots<3, Lx, Tx, Ly, Ty> {

  using parent = WithRoots<3, Lx, Tx, Ly, Ty>;

public:
  using parent::WithRoots; // Inherit constructors
  Polynomial3(const parent &p) : parent(p) {}

  /// Coefficient A (of degree 3)
  const Quantity<Ly - 3 * Lx, Ty - 3 * Tx> &coeffA() const {
    return parent::template coeff<3>();
  }
  /// Coefficient B (of degree 2)
  const Quantity<Ly - 2 * Lx, Ty - 2 * Tx> &coeffB() const {
    return parent::template coeff<2>();
  }
  /// Coefficient C (of degree 1)
  const Quantity<Ly - Lx, Ty - Tx> &coeffC() const {
    return parent::template coeff<1>();
  }
  /// Coefficient D (of degree 0)
  const Quantity<Ly, Ty> &coeffD() const { return parent::template coeff<0>(); }

  std::vector<Quantity<Lx, Tx>> roots() const override {

    if (coeffA().isNull()) {
      return Polynomial2<Lx, Tx, Ly, Ty>(parent::template degree_cast<2>())
          .roots();
    }

    const auto asquare = coeffA().square();
    const Quantity<2 * Lx, 2 *Tx> p =
        (3 * coeffA() * coeffC() - coeffB().square()) / (3 * asquare);
    const Quantity<3 * Lx, 3 *Tx> q =
        (2 * coeffB().cube() - 9 * coeffA() * coeffB() * coeffC() +
         27 * asquare * coeffD()) /
        (27 * coeffA().cube());

    const Quantity<6 * Lx, 6 *Tx> delta1 = q.square() + 4 * p.cube() / 27;
    const Quantity<3 * Lx, 3 *Tx> delta1sqrt = delta1.sqrt();

    const auto x1 = ((-q - delta1sqrt) / 2).cbrt() +
                    ((-q + delta1sqrt) / 2).cbrt() - coeffB() / (3 * coeffA());

    const Polynomial2<Lx, Tx, Ly - Lx, Ty - Tx> subpolynomial(
        coeffA(), coeffB() + coeffA() * x1,
        coeffC() + (coeffB() + coeffA() * x1) * x1);
    const std::vector<Quantity<Lx, Tx>> sol2 = subpolynomial.roots();

    std::vector<Quantity<Lx, Tx>> solutions;
    solutions.push_back(x1);
    solutions.push_back(sol2[0]);
    solutions.push_back(sol2[1]);

    return solutions;
  }

  const Polynomial3<Lx, Tx, Ly, Ty> operator-() const {
    return Polynomial3<Lx, Tx, Ly, Ty>(parent::operator-());
  }
  const Polynomial3<Lx, Tx, Ly, Ty>
  operator+(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial3<Lx, Tx, Ly, Ty>(parent::operator+(rhs));
  }
  const Polynomial3<Lx, Tx, Ly, Ty>
  operator-(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial3<Lx, Tx, Ly, Ty>(parent::operator-(rhs));
  }
  const Polynomial3<Lx, Tx, Ly, Ty> operator*(const Scalar &rhs) const {
    return Polynomial3<Lx, Tx, Ly, Ty>(parent::operator*(rhs));
  }
  const Polynomial3<Lx, Tx, Ly, Ty> operator/(const Scalar &rhs) const {
    return Polynomial3<Lx, Tx, Ly, Ty>(parent::operator/(rhs));
  }
};

template <int Lx, int Tx, int Ly, int Ty>
const Polynomial3<Lx, Tx, Ly, Ty>
operator*(const Scalar &lhs, const Polynomial3<Lx, Tx, Ly, Ty> &rhs) {
  return Polynomial3(lhs * (Polynomial<3, Lx, Tx, Ly, Ty>)rhs);
}
