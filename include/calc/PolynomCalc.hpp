// -*- lsst-c++ -*-

#ifndef _POLYNOMCALC_HPP_
#define _POLYNOMCALC_HPP_

#include "calc/Quantity.hpp"
#include "calc/Tuple.hpp"
#include <stdexcept>
#include <vector>
#ifdef _DEBUG
#include <iostream>
#endif

template <int D, int Lx, int Tx, int Ly, int Ty> class Polynomial;

/**
 * The zero polynomial. -1 is used for the degree -infinity.
 *
 * @tparam Lx,Tx Dimensions of x in P(x), where P is this polynomial.
 * @tparam Ly,Ty Dimensions of P(x) in P(x), where P is this polynomial and x
 * has the dimensions (Lx,Tx)
 */
template <int Lx, int Tx, int Ly, int Ty>
class Polynomial<-1, Lx, Tx, Ly, Ty> : tuple<> {

  friend class Polynomial<0, Lx, Tx, Ly, Ty>;

private:
  using type = tuple<>;
  using Self = Polynomial<-1, Lx, Tx, Ly, Ty>;
  Polynomial(const Quantity<Ly, Ty> &_nul, const Self &_p) {
    if (!_nul.isNull()) {
      throw std::logic_error(
          "A zero polynomial only accepts coefficient zero.");
    }
  }

public:
  using type::tuple; // Inherit constructors
  Polynomial(const type &coeffs) {}

  const Self forgetGreatestDegree() { return *this; }

  const Quantity<Ly, Ty> eval(const Quantity<Lx, Tx> &point) const {
    return Quantity<Ly, Ty>(0);
  }
  const Self derivative() const { return *this; }

  const int getActualDegree() const { return -1; }

  template <int D>
  typename std::enable_if<(D == -1), const Self>::type degree_cast() const {
    return *this;
  }

  template <int D>
  typename std::enable_if<(D > -1), const Polynomial<D, Lx, Tx, Ly, Ty>>::type
  degree_cast() const {
    return Polynomial<D, Lx, Tx, Ly, Ty>(Quantity<Ly - D * Lx, Ty - D * Ly>(0),
                                         degree_cast<D - 1>());
  }

  template <unsigned int K>
  [[deprecated("All coefficients of a zero polynomial are "
               "zero.")]] const Quantity<Ly - (int)K * Lx, Ty - (int)K * Tx>
  coeff() const {
    return Quantity<Ly - (int)K * Lx, Ty - (int)K * Tx>(0);
  }

  const Self operator-() const { return *this; }
  const Self operator*(const Scalar &rhs) const { return *this; }
  const Self operator/(const Scalar &rhs) const { return *this; }
  const Polynomial<0, Lx, Tx, Ly, Ty>
  operator+(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial<0, Lx, Tx, Ly, Ty>(rhs);
  }
  const Polynomial<0, Lx, Tx, Ly, Ty>
  operator-(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial<0, Lx, Tx, Ly, Ty>(-rhs);
  }

  template <int D>
  const Polynomial<D, Lx, Tx, Ly, Ty>
  operator+(const Polynomial<D, Lx, Tx, Ly, Ty> &rhs) const {
    return rhs;
  }

  template <int D>
  const Polynomial<D, Lx, Tx, Ly, Ty>
  operator-(const Polynomial<D, Lx, Tx, Ly, Ty> &rhs) const {
    return rhs;
  }
};

/**
 * A polynomial of degree at most D to use with physical quantities.
 *
 * @tparam D The maximum (compile-time) degree of the polynomial must be
 * positive, null or -1.
 * (-1 is used for the degree -infinity.)
 * @tparam Lx,Tx Dimensions of x in P(x), where P is this polynomial.
 * @tparam Ly,Ty Dimensions of P(x) in P(x), where P is this polynomial and x
 * has the dimensions (Lx,Tx)
 */
template <int D, int Lx, int Tx, int Ly, int Ty>
class Polynomial
    : flatten<Quantity<Ly - D * Lx, Ty - D * Tx>,
              typename Polynomial<D - 1, Lx, Tx, Ly, Ty>::type>::type {

  friend class Polynomial<D + 1, Lx, Tx, Ly, Ty>;

private:
  /// The polynomial of degree D-1
  using inferior = Polynomial<D - 1, Lx, Tx, Ly, Ty>;
  using flat =
      flatten<Quantity<Ly - D * Lx, Ty - D * Tx>, typename inferior::type>;
  /**
   * The tuple type this polynomial extends.
   * This should exactly match the inherited type.
   */
  using type = typename flat::type;
  /// Convenience shortcut typedef to refer to the type of this polynomial.
  using Self = Polynomial<D, Lx, Tx, Ly, Ty>;

  /**
   * Eval a truncation at degree K of this polynomial, at the specified point.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K > -1), const Quantity<Ly, Ty>>::type
  __eval_impl(const Quantity<Lx, Tx> &point) const {
    return coeff<K>() * point.template pow<K>() + __eval_impl<K - 1>(point);
  }

  /**
   * Eval a truncation at degree -1 of this polynomial, at the specified point.
   * Returns 0 as the polynomial of degree -1 is the zero polynomial.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K == -1), const Quantity<Ly, Ty>>::type
  __eval_impl(const Quantity<Lx, Tx> &point) const {
    return Quantity<Ly, Ty>(0);
  }

  /**
   * Return the degree of a truncation at degree K of this polynomial.
   * Internal use only.
   */
  template <unsigned int K> int __degree_impl() const {
    if (!coeff<K>().isNull()) {
      return K;
    }
    return __degree_impl<K - 1>();
  }

  /**
   * Return a polynomial with the same dimension specification as this instance,
   * but a degree reduced by 1.
   * The returned polynomial will have the same coefficients as this instance
   * except for the degree K.
   *
   * This instance is untouched.
   */
  inferior truncateGreatestDegree() const { return inferior(std::tail(*this)); }

  /**
   * Add a truncation of this polynomial at degree K to rhs.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K > 0), const Polynomial<K, Lx, Tx, Ly, Ty>>::type
  __impl_operator_plus(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial<K, Lx, Tx, Ly, Ty>(coeff<K>(),
                                         __impl_operator_plus<K - 1>(rhs));
  }

  /**
   * Add a truncation of this polynomial at degree 0 to rhs.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K == 0), const Polynomial<K, Lx, Tx, Ly, Ty>>::type
  __impl_operator_plus(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial<K, Lx, Tx, Ly, Ty>(coeff<K>() + rhs);
  }

public:
  using type::tuple; // Inherit constructors

  /**
   * Construct a new polynomial with the specified coefficients.
   * The first coefficient is the highest-degree one.
   */
  Polynomial(const type &coeffs) : type(coeffs) {}
  /**
   * Construct a new polynomial by appending a coefficient to a base polynomial.
   * The resulting polynomial is equal to (main_coeff * X^K + base)
   */
  Polynomial(const Quantity<Ly - D * Lx, Ty - D * Tx> &main_coeff,
             const inferior &base)
      : Polynomial(flat::build(main_coeff, base)) {}

#ifdef _DEBUG
  /**
   * Print the coefficient of degree 0 in os.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K == 0), std::ostream &>::type
  fprint(std::ostream &os) const {
    return os << coeff<0>();
  }

  /**
   * Print a truncation at degree K of this polynomial.
   * Internal use only.
   */
  template <int K>
  typename std::enable_if<(K > 0), std::ostream &>::type
  fprint(std::ostream &os) const {
    return fprint<K - 1>(os << coeff<K>() << "x^" << K << " + ");
  }
#endif

  /**
   * Return the coefficient of degree K of this polynomial.
   */
  template <unsigned int K>
  typename std::enable_if<
      (K <= D), const Quantity<Ly - (int)K * Lx, Ty - (int)K * Tx>>::type &
  coeff() const {
    return std::get<D - K>(*this);
  }

  /**
   * Return the coefficient of degree K of this polynomial.
   *
   * @deprecated All coefficients of degree greater than this polynomial will be
   * null.
   */
  template <unsigned int K>
  [[deprecated(
      "All coefficients of degree greater than this polynomial will be null.")]]
  typename std::enable_if<
      (K > D), const Quantity<Ly - (int)K * Lx, Ty - (int)K * Tx>>::type
  coeff() const {
    return Quantity<Ly - (int)K * Lx, Ty - (int)K * Tx>(0);
  }

  /**
   * Returns the actual degree of this polynomial.
   * This degree is guaranteed to be lower than or equal to D.
   */
  int getActualDegree() const { return __degree_impl<D>(); }

  /**
   * Change the compile-time degree of this polynomial to D2.
   * This instance is untouched.
   *
   * @tparam D2 the new compile-time degree of the polynomial.
   * @throws If D2 is smaller than the actual degree of this polynomial.
   * This can only happen if D2 < D.
   */
  template <int D2>
  typename std::enable_if<(D2 == D), const Self>::type degree_cast() const {
    return *this;
  }

  /**
   * Change the compile-time degree of this polynomial to D2.
   * This instance is untouched.
   *
   * @tparam D2 the new compile-time degree of the polynomial.
   * @throws If D2 is smaller than the actual degree of this polynomial.
   * This can only happen if D2 < D.
   */
  template <int D2>
  typename std::enable_if<(D2 > D), const Polynomial<D2, Lx, Tx, Ly, Ty>>::type
  degree_cast() const {
    return Polynomial<D2, Lx, Tx, Ly, Ty>(
        Quantity<Ly - D2 * Lx, Ty - D2 * Ly>(0), degree_cast<D2 - 1>());
  }

  /**
   * Change the compile-time degree of this polynomial to D2.
   * This instance is untouched.
   *
   * @tparam D2 the new compile-time degree of the polynomial.
   * @throws If D2 is smaller than the actual degree of this polynomial.
   * This can only happen if D2 < D.
   */
  template <int D2>
  typename std::enable_if<(D2 >= -1 && D2 < D),
                          const Polynomial<D2, Lx, Tx, Ly, Ty>>::type
  degree_cast() const {
    if (!coeff<D>().isNull()) {
      throw std::logic_error("Polynomial: Cannot cast this polynomial to a "
                             "lower degree than its actual degree.");
    }
    return truncateGreatestDegree().template degree_cast<D2>();
  }

  /**
   * Evaluate this polynomial at the given point and return the result.
   */
  const Quantity<Ly, Ty> eval(const Quantity<Lx, Tx> &point) const {
    return __eval_impl<D>(point);
  }

  /**
   * Shortcut for eval(Quantity).
   */
  const Quantity<Ly, Ty> operator()(const Quantity<Lx, Tx> &point) const {
    return eval(point);
  }
  /**
   * Get the derivative of this polynomial.
   * This instance is untouched.
   */
  const inferior derivative() const {
    return inferior(D * coeff<D>(), truncateGreatestDegree().derivative());
  }

  const Self operator-() const {
    return Self(-coeff<D>(), -truncateGreatestDegree());
  }

  const Self operator+(const Quantity<Ly, Ty> &rhs) const {
    return __impl_operator_plus<D>(rhs);
  }

  const Self operator-(const Quantity<Ly, Ty> &rhs) const {
    return *this + (-rhs);
  }

  const Self operator*(const Scalar &rhs) const {
    return Self(coeff<D>() * rhs, truncateGreatestDegree() * rhs);
  }
  const Self operator/(const Scalar &rhs) const {
    return Self(coeff<D>() / rhs, truncateGreatestDegree() / rhs);
  }

  template <int D2>
  typename std::enable_if<(D == D2), const Self>::type
  operator+(const Polynomial<D2, Lx, Tx, Ly, Ty> &rhs) const {
    return Self(coeff<D>() + rhs.template coeff<D>(),
                truncateGreatestDegree() + rhs.truncateGreatestDegree());
  }

  template <int D2>
  typename std::enable_if<(D < D2), const Polynomial<D2, Lx, Tx, Ly, Ty>>::type
  operator+(const Polynomial<D2, Lx, Tx, Ly, Ty> &rhs) const {
    return rhs + *this;
  }

  template <int D2>
  typename std::enable_if<(D2 < 0), const Self>::type
  operator+(const Polynomial<D2, Lx, Tx, Ly, Ty> &rhs) const {
    return *this;
  }

  template <int D2>
  typename std::enable_if<(D2 >= 0 && D2 < D), const Self>::type
  operator+(const Polynomial<D2, Lx, Tx, Ly, Ty> &rhs) const {
    return *this + rhs.template degree_cast<D>();
  }

  template <int D2>
  const Polynomial<std::max(D, D2), Lx, Tx, Ly, Ty>
  operator-(const Polynomial<D2, Lx, Tx, Ly, Ty> &rhs) const {
    return *this + (-rhs);
  }
};

template <int D, int Lx, int Tx, int Ly, int Ty>
const Polynomial<D, Lx, Tx, Ly, Ty>
operator*(const Scalar &lhs, const Polynomial<D, Lx, Tx, Ly, Ty> &rhs) {
  return rhs * lhs;
}

#ifdef _DEBUG
template <int D, int Lx, int Tx, int Ly, int Ty>
std::ostream &operator<<(std::ostream &os,
                         const Polynomial<D, Lx, Tx, Ly, Ty> &rhs) {
  return rhs.template fprint<D>(os);
}
template <int Lx, int Tx, int Ly, int Ty>
std::ostream &operator<<(std::ostream &os,
                         const Polynomial<-1, Lx, Tx, Ly, Ty> &rhs) {
  return os << "0";
}
#endif

/**
 * Base class for polynomials that have (a finite number of) roots and
 * implement a method to retrieve them.
 *
 * This class must be inherited.
 *
 * @tparam D The maximum degree of this polynomial
 * @tparam Lx,Tx Dimensions of x in P(x), where P is this polynomial.
 * @tparam Ly,Ty Dimensions of P(x) in P(x), where P is this polynomial and x
 * has the dimensions (Lx,Tx)
 */
template <int D, int Lx, int Tx, int Ly, int Ty>
class WithRoots : public Polynomial<D, Lx, Tx, Ly, Ty> {

  using Polynomial<D, Lx, Tx, Ly, Ty>::Polynomial;

public:
  WithRoots(const Polynomial<D, Lx, Tx, Ly, Ty> &value)
      : Polynomial<D, Lx, Tx, Ly, Ty>::Polynomial(value) {}

  /**
   * Get the list of the (complex) roots of the polynomial,
   * in an unspecified order.
   * If this polynomial is actually the zero polynomial, the result
   * is unspecified.
   *
   * This method must be overriden.
   */
  virtual std::vector<Quantity<Lx, Tx>> roots() const = 0;

  /**
   * Get the list of the real roots of the polynomial,
   * in increasing order.
   *
   * The non-real roots are ignored.
   */
  std::vector<Quantity<Lx, Tx>> realRoots() {
    std::vector<Quantity<Lx, Tx>> lst = roots();
    std::vector<Quantity<Lx, Tx>> copy;

    for (const Quantity<Lx, Tx> &q : lst) {
      if (q.isReal()) {
        copy.push_back(q);
      }
    }
    sort(copy.begin(), copy.end());
    return copy;
  }

  /**
   * Get the list of the real and strictly positive roots of the polynomial,
   * in increasing order.
   */
  std::vector<Quantity<Lx, Tx>> positiveRoots() const {

    std::vector<Quantity<Lx, Tx>> lst = roots();
    std::vector<Quantity<Lx, Tx>> copy;

    for (const Quantity<Lx, Tx> &q : lst) {
      if (q.isStrictlyPositive()) {
        copy.push_back(q);
      }
    }
    sort(copy.begin(), copy.end());
    return copy;
  }
};

#include "calc/Polynom1.hpp"
#include "calc/Polynom2.hpp"
#include "calc/Polynom3.hpp"
#include "calc/Polynom4.hpp"

#endif
