// -*- lsst-c++ -*-

#ifndef _POLYNOMCALC_HPP_
#error "Please include 'PolynomCalc.hpp' instead."
#endif
#include "calc/PolynomCalc.hpp" //For the IDE only

/**
 * A polynomial of degree (at most) 2 that provides
 * a method to retrieve its roots.
 *
 * @tparam Lx,Tx,Ly,Ty @see Polynomial @see WithRoots
 */
template <int Lx, int Tx, int Ly, int Ty>
class Polynomial2 : public WithRoots<2, Lx, Tx, Ly, Ty> {
private:
  using parent = WithRoots<2, Lx, Tx, Ly, Ty>;

public:
  using parent::WithRoots; // Inherit constructors
  Polynomial2(const parent &p) : parent(p) {}

  /// Coefficient A (of degree 2)
  const Quantity<Ly - 2 * Lx, Ty - 2 * Tx> &coeffA() const {
    return parent::template coeff<2>();
  }
  /// Coefficient B (of degree 1)
  const Quantity<Ly - Lx, Ty - Tx> &coeffB() const {
    return parent::template coeff<1>();
  }
  /// Coefficient C (of degree 0)
  const Quantity<Ly, Ty> &coeffC() const { return parent::template coeff<0>(); }

  std::vector<Quantity<Lx, Tx>> roots() const override {
    if (coeffA().isNull()) {
      return Polynomial1<Lx, Tx, Ly, Ty>(parent::template degree_cast<1>())
          .roots();
    }

    const auto delta = (coeffB().square() - 4 * coeffA() * coeffC());
    const auto delta_sqrt = delta.sqrt();

    std::vector<Quantity<Lx, Tx>> solutions;
    solutions.push_back((-coeffB() - delta_sqrt) / (2 * coeffA()));
    solutions.push_back((-coeffB() + delta_sqrt) / (2 * coeffA()));

    return solutions;
  }

  const Polynomial2<Lx, Tx, Ly, Ty> operator-() const {
    return Polynomial2<Lx, Tx, Ly, Ty>(parent::operator-());
  }
  const Polynomial2<Lx, Tx, Ly, Ty>
  operator+(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial2<Lx, Tx, Ly, Ty>(parent::operator+(rhs));
  }
  const Polynomial2<Lx, Tx, Ly, Ty>
  operator-(const Quantity<Ly, Ty> &rhs) const {
    return Polynomial2<Lx, Tx, Ly, Ty>(parent::operator-(rhs));
  }
  const Polynomial2<Lx, Tx, Ly, Ty> operator*(const Scalar &rhs) const {
    return Polynomial2<Lx, Tx, Ly, Ty>(parent::operator*(rhs));
  }
  const Polynomial2<Lx, Tx, Ly, Ty> operator/(const Scalar &rhs) const {
    return Polynomial2<Lx, Tx, Ly, Ty>(parent::operator/(rhs));
  }
};

template <int Lx, int Tx, int Ly, int Ty>
const Polynomial2<Lx, Tx, Ly, Ty>
operator*(const Scalar &lhs, const Polynomial2<Lx, Tx, Ly, Ty> &rhs) {
  return Polynomial2(lhs * (Polynomial<2, Lx, Tx, Ly, Ty>)rhs);
}