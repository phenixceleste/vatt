// -*- lsst-c++ -*-

#include "tests/TestJump.hpp"
#include "objects/Player.hpp"
#include <iostream>

TestJump::TestJump() : Test("Jump") {

  DURATION_THRESHOLD = 0.3;
  HEIGHT_THRESHOLD = 3;

  EXP_H_MIN = JumpTrajectory::H_MIN.requireRealValue();
  EXP_H_MAX = JumpTrajectory::H_MAX.requireRealValue();

  T_1 = JumpTrajectory::getCriticalJumpTime(Time(0)).requireRealValue();
  T_2 = JumpTrajectory::getCriticalJumpTime(JumpTrajectory::T_1)
            .requireRealValue();

  SMALL_DURATION = JumpTrajectory::getMinJumpLandingTime(Length(0), Length(0))
                       .requireRealValue();
  LONG_DURATION = JumpTrajectory::getMaxJumpLandingTime(Length(0), Length(0))
                      .requireRealValue();
}

bool TestJump::onBeforeRun(GameManager &game) {
  game.stopRunning();
  m_step = STEP_SMALL_JUMP;
  m_successful = true;
  return Test::onBeforeRun(game);
}

void TestJump::startJump(GameManager &game) {
  m_jump_start = game.getElapsedTime();
  m_max_reach_time = 0;
  m_init_y = game.getPlayer(0).getY();
  m_jump_height = m_init_y;

  game.getPlayer(0).startJump();

  m_step++;
}

bool TestJump::updateJumpLog(GameManager &game) {
  OptionalRef<const Platform &> platform;
  const Player &player = game.getPlayer(0);
  if (game.isOnAPlatform(player, platform)) {
    return true;
  } else {
    double y = player.getY();
    if (y < m_jump_height) {
      m_jump_height = y;
      m_max_reach_time = game.getElapsedTime();
    }
    return false;
  }
}

bool TestJump::printJumpDetails(GameManager &game, double expectedDuration,
                                double expectedJumpHeight,
                                double expectedCriticalTime) {

  double duration = game.getElapsedTime() - m_jump_start;
  m_max_reach_time = m_max_reach_time - m_jump_start;
  m_jump_height = m_jump_height - m_init_y;

  std::cout << "Jump lasted: " << duration << " (expected: " << expectedDuration
            << ")" << std::endl;
  std::cout << "Jump height: " << m_jump_height
            << " (expected: " << expectedJumpHeight << ")" << std::endl;
  std::cout << "Maximum reached in " << m_max_reach_time
            << " (expected: " << expectedCriticalTime << ")" << std::endl;

  return std::abs(duration - expectedDuration) < DURATION_THRESHOLD &&
         std::abs(m_jump_height - expectedJumpHeight) < HEIGHT_THRESHOLD &&
         std::abs(m_max_reach_time - expectedCriticalTime) < DURATION_THRESHOLD;
}

bool TestJump::onTick(GameManager &game) {

  switch (m_step) {
  case STEP_SMALL_JUMP:
  case STEP_LONG_JUMP:
    startJump(game);
    break;
  case STEP_SMALL_JUMP_IN_PROGRESS:
    game.getPlayer(0).stopJump();
    if (updateJumpLog(game)) {
      std::cout << "-- SMALL JUMP COMPLETE --" << std::endl;
      m_successful = printJumpDetails(game, SMALL_DURATION, EXP_H_MIN, T_1) &&
                     m_successful;
      m_step++;
    }
    break;
  case STEP_LONG_JUMP_IN_PROGRESS:
    if (updateJumpLog(game)) {
      std::cout << "-- LONG JUMP COMPLETE --" << std::endl;
      m_successful =
          printJumpDetails(game, LONG_DURATION, EXP_H_MAX, T_2) && m_successful;
      m_step++;
    }
    break;
  default:
    // Test complete.
    return false;
  }
  return true;
}

bool TestJump::wasSuccessfull(GameManager &game) { return m_successful; }