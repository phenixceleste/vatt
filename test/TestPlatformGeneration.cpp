// -*- lsst-c++ -*-

#include "tests/TestPlatformGeneration.hpp"
#include "objects/Player.hpp"
#include <iostream>

TestPlatformGeneration::TestPlatformGeneration() : Test("PlatformGeneration") {}

void TestPlatformGeneration::detailFailure(GameManager &game) const {
  std::cout << "-- SIMULATION FAILED AFTER UNEXPECTED EVENT --" << std::endl
            << (m_expected_trajectory->shouldJump() ? "Should have jumped"
                                                    : "Should have fallen")
            << std::endl
            << "Score: " << game.getScore() << std::endl
            << "Expected gliding time "
            << m_expected_trajectory->getExpectedGlidingTime() << std::endl
            << "Expected landing time "
            << m_expected_trajectory->getExpectedLandingTime() << std::endl;
}

bool TestPlatformGeneration::onTick(GameManager &game, const int &time_limit) {

  m_successful = false;
  if (!game.isAlive()) {
    detailFailure(game);
    return false;
  }

  Player &player = game.getPlayer(0);
  OptionalRef<const Platform &> platform;

  if (game.isOnAPlatform(player, platform)) {
    // On a platform:
    // Store the expected trajectory when the player will leave the platform
    // Jump if needed, or do nothing

    m_expected_trajectory = platform->getJumpTrajectory();

    bool should_jump = m_expected_trajectory->shouldJump();
    if (should_jump) {
      const double iota = game.getElapsedTime();
      const double T_init =
          m_expected_trajectory->getBeginningOfEvent().requireRealValue();
      if (iota >= T_init) {
        player.startJump();
      }
    }

  } else if (m_expected_trajectory->shouldJump()) {
    // Not on a platform
    // Check if the player should stop gliding now
    // Or do nothing

    const double iota = game.getElapsedTime();
    const Time T_init = m_expected_trajectory->getBeginningOfEvent();
    const Time t_0 = m_expected_trajectory->getExpectedGlidingTime();

    if (iota >= (T_init + t_0).requireRealValue()) {
      player.stopJump();
    }
  }

  m_successful = time_limit > 0 && game.getElapsedTime() > time_limit;
  return !m_successful;
}

bool TestPlatformGeneration::wasSuccessfull(GameManager &game,
                                            const int &time_limit) {
  return m_successful;
}