

# variables

# where we put all our binaries and other similar files
BUILDDIR:= target

# the files to compile
# ADD NEWLY CREATED FILES HERE:
override SRC :=	\
		src/main.cpp \
		src/util.cpp \
		src/GameManager.cpp \
		src/Session.cpp \
		src/GameWindow.cpp \
		\
		src/objects/GameObject.cpp \
		src/objects/DisplayableEntity.cpp \
		src/objects/PlaneEntity.cpp \
		src/objects/CircleEntity.cpp \
		src/objects/RectangleEntity.cpp \
		\
		src/objects/Player.cpp \
		src/objects/Platform.cpp \
		src/objects/Coin.cpp \
		\
		src/components/Component.cpp\
		src/components/GraphicComponent.cpp \
		src/components/SpeedModel.cpp \
		\
		src/generators/PlatformGenerator.cpp \
		src/generators/CoinGenerator.cpp \
		src/generators/MapGenerator.cpp \
		src/generators/JumpTrajectory.cpp \
		\
		src/Collectible.cpp \
		src/Action.cpp \
		src/Transform.cpp \
		src/TimeManager.cpp \
		\
		src/menu/IMenu.cpp \
		src/menu/IMenuItem.cpp \
		src/menu/GameMenu.cpp \
		src/menu/ContinueGameMenuItem.cpp \
		src/menu/NewGameMenuItem.cpp \
		src/menu/QuitMenuItem.cpp \
		src/menu/SaveReplayMenuItem.cpp \
		src/menu/WatchReplayMenuItem.cpp

#test files to compile
#only compiled when using "make test"
# ADD NEWLY CREATED TESTS HERE:
override TESTS := \
		test/CommandLineParser.cpp \
		test/TestPlatformGeneration.cpp \
		test/TestJump.cpp


# the corresponding binary files
# they are obtained by replacing (syntactically) all instances of .cpp by .o
override OBJ :=	$(SRC:%.cpp=$(BUILDDIR)/%.o)
override TESTS_OBJ := $(TESTS:%.cpp=$(BUILDDIR)/%.o)

# name of the project
NAME := vatt

# compiler
CXX:= g++ # it is g++ by default, but good to know the option
# pre-compilation flags
override CPPFLAGS += -I include
# compile flags
override CXXFLAGS += -Wall -std=c++17

# linker (g++ is fine, just in case we want it to be custom)
LD := $(CXX)
# libraries used in the project
override LDLIBS += -lsfml-system -lsfml-graphics -lsfml-window
# flags for the linkage
override LDFLAGS +=

# lmsfhqsdmjf
override RM := rm -rf

CYELLOW := $(shell echo -e "\033[1;33m")
CRED := $(shell echo -e "\033[1;31m")
CRESET := $(shell echo -e "\033[0m")

# the default rule for the makefile (must be the first one)
all: $(NAME)

debug: override CPPFLAGS += -D_DEBUG
debug: override CXXFLAGS += -g
debug: $(NAME)
	$(warning $(CYELLOW) WARNING: \
	You've compiled a debug binary. Make sure to clean \
	the project before building a release. $(CRESET))

test: override CPPFLAGS += -D_TEST -D_DEBUG
test: override CXXFLAGS += -g
test: $(NAME)_test
	$(warning $(CYELLOW)WARNING: \
	You've compiled the sources with tests included. \
	The binary also contains debug information. \
	Make sure to clean the project before building a release. \
	$(CRESET))

# main rule for the project
# the $^ sign means that all dependencies are compiled
# the -o option refers to the linkage step
# $@ means the resulting binary will be named NAME
$(NAME): $(OBJ)
	@if [ -d "target/test" ]; then \
		echo "$(CRED)ERROR: Your build is corrupt \
(tests unexpectedly found under ./target/). \
You must clean the project ('make clean') before building a release. \
				$(CRESET)"; \
				exit 1; \
	fi
	$(LD) $(LDFLAGS) $^ -o $@ $(LDLIBS) 

$(NAME)_test: $(OBJ) $(TESTS_OBJ)
	$(LD) $(LDFLAGS) $^ -o $@ $(LDLIBS) 

# cleans up everything nicely
clean:
	$(RM) $(BUILDDIR) $(NAME) $(NAME).exe $(NAME)_test $(NAME)_test.exe save

# formats the code
fmt:
	find . src -name "*.cpp" -exec clang-format -i --Werror {} +
	find . include -name "*.hpp" -exec clang-format -i --Werror {} +
	find . test -name "*.cpp" -exec clang-format -i -Werror {} +

# rules in order to correctly take into account the .hpp files
$(BUILDDIR)/%.o: override CPPFLAGS += -MT $@ -MMD -MP -MF $(@:.o=.d)
$(BUILDDIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(COMPILE.cc) $< -o $@


# the targets that do not generate binaries
.PHONY := all clean debug test

-include $(OBJ:.o=.d)

