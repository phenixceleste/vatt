// -*- lsst-c++ -*-

#include "Session.hpp"

Session::Session(std::string filename) : m_filename(filename) {}

Session::Session(std::vector<std::string> &stringVect) {
  m_filename = stringVect[0];
  m_bestScore = stol(stringVect[1]);
  m_coins = stol(stringVect[2]);
}

Session Session::fromFile(std::string filename) {

  std::vector<std::string> stringVect(1, filename);

  std::string line;
  if (std::filesystem::exists(filename)) {
    std::ifstream file(filename);
    if (file.is_open()) {
      while (getline(file, line)) {
        stringVect.push_back(line);
      }
      file.close();
      return Session(stringVect);
    }
    return Session(filename);
  }

  return Session(filename);
}

Session Session::temp() { return Session(""); }

void Session::updateBestScore(long score) {
  if (score > m_bestScore)
    m_bestScore = score;
}
void Session::addCoin(long amount) { m_coins += amount; }

long Session::getBestScore() { return m_bestScore; }
long Session::getCoins() { return m_coins; }

void Session::save() {
  if (m_filename == "") {
    return;
  }
  std::ofstream file(m_filename);
  std::vector<std::string> stringVect;
  stringVect.push_back(std::to_string(m_bestScore));
  stringVect.push_back(std::to_string(m_coins));
  file << linefeedFold(stringVect);
  file.close();
}