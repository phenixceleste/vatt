// -*- lsst-c++ -*-

#include "Collectible.hpp"

Collectible::Collectible() {}

void Collectible::markCollected() { m_collected = true; }
bool Collectible::isCollected() const { return m_collected; }
