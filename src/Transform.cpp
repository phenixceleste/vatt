#include <cmath>
#include <tuple>

#include "Transform.hpp"

Transform::Transform() {}

std::tuple<float, float> Transform::getPos() {
  return std::tuple<float, float>(m_x_pos, m_y_pos);
}

void Transform::setPos(float new_x_pos, float new_y_pos) {
  m_x_pos = new_x_pos;
  m_y_pos = new_y_pos;
}

float Transform::getRot() { return m_rotation; }

void Transform::setRot(float new_rot) {
  m_rotation = std::fmod(new_rot, 360.0);
}
