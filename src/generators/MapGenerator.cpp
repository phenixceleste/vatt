// -*- lsst-c++ -*-

#include "generators/MapGenerator.hpp"

MapGenerator::MapGenerator(GameManager &gameManager)
    : m_platform_generator(gameManager), m_coin_generator(gameManager) {}

void MapGenerator::generateMap() {
  m_platform_generator.generatePlatform();
  m_coin_generator.generateCoin();
}