// -*- lsst-c++ -*-

#include "generators/CoinGenerator.hpp"
#include "GameManager.hpp"

CoinGenerator::CoinGenerator(GameManager &gameManager) : m_board(gameManager) {}

void CoinGenerator::generateCoin() {
  const JumpTrajectory &trajectory = m_board.getLastJumpTrajectory();
  const Time T_p = trajectory.getExpectedLandingTime();

  if (m_board.randDouble(0, 1) < 0.4 and T_p.requireRealValue() > 0.1 + 0.3) {
    const double t_coin = m_board.randDouble(0.1, T_p.requireRealValue() - 0.3);

    const double x = m_board.getLocalX(
        Length(trajectory.getX(m_board.getGameSpeedModel(), t_coin)));
    const double y = trajectory.getY(t_coin);

    Coin newCoin(x + GameManager::PLAYER_WIDTH / 2 - Coin::COIN_RADIUS,
                 y - GameManager::PLAYER_HEIGHT / 2 - Coin::COIN_RADIUS);
    m_board.addCoin(newCoin);
  }

  else if (m_board.randDouble(0, 1) < 0.5) {
    const Platform &platform = m_board.getLastPlatform();
    double left = platform.getX();
    double right = platform.getRight();
    double y = platform.getY();
    Coin newCoin(m_board.randDouble(
                     left + GameManager::PLAYER_WIDTH - Coin::COIN_RADIUS,
                     right - GameManager::PLAYER_WIDTH - Coin::COIN_RADIUS),
                 y - GameManager::PLAYER_HEIGHT / 2 - Coin::COIN_RADIUS);
    m_board.addCoin(newCoin);
  }
}
