// -*- lsst-c++ -*-

#include "generators/PlatformGenerator.hpp"
#include "GameManager.hpp"
#include <chrono>
#include <tuple>
#include <unistd.h>

const Length PlatformGenerator::PLAYER_MAX_Y =
    Length(GameManager::TOTAL_VERTICAL_DIV * 0.8);
const Length PlatformGenerator::PLAYER_MIN_Y =
    Length(GameManager::TOTAL_VERTICAL_DIV / 3);

template <int L, int T>
const Quantity<L, T>
PlatformGenerator::randQuantity(const Quantity<L, T> &min,
                                const Quantity<L, T> &max) {
  return Quantity<L, T>(
      m_board.randDouble(min.requireRealValue(), max.requireRealValue()));
}

void PlatformGenerator::fixCollisions(JumpTrajectory &jumpTrajectory) const {

  const ISpeedModel &speedModel = m_board.getGameSpeedModel();
  const Time epsilon = Time(5e-1);

  const Length w = Length(GameManager::PLAYER_WIDTH);

  const Time t_c = jumpTrajectory.getCriticalTime();
  const Time t_P = jumpTrajectory.getExpectedLandingTime();
  const Length X_p = jumpTrajectory.getX(speedModel, t_P);
  const Time T_init = jumpTrajectory.getBeginningOfEvent();
  const Time t_x = speedModel.getTime(X_p - w) - T_init;

  if (t_x <= t_c + epsilon) {
    const Time T_p2 =
        speedModel.getTime(jumpTrajectory.getX(speedModel, t_c) + w) - T_init +
        epsilon;
    jumpTrajectory.setExpectedLandingTime(T_p2);
  }
}

void PlatformGenerator::generatePlatform() {

  // SEE THE PDF FOR EXPLANATION OF THE METHOD AND THE IMPLEMENTATION.

  // ------------------------------------------------

  // The function relies on the template class Quantity<L, T> (and Scalar,
  // Length, Time, Speed, Acceleration) to provide compile-time dimensional
  // analysis in order to ensure that all the physical formulas are homogeneous
  // (see include/calc/Quantity.hpp).

  const ISpeedModel &speedModel = m_board.getGameSpeedModel();
  const Platform &last_platform = m_board.getLastPlatform();

  const Length y_0 = Length(last_platform.getY());

  // 1)
  Length min_y = y_0 + JumpTrajectory::H_MAX;
  if (min_y < PLAYER_MIN_Y) {
    min_y = PLAYER_MIN_Y;
  }
  Length Y_p = randQuantity(min_y, PLAYER_MAX_Y);

  // 2)
  bool can_avoid_jump = Y_p > y_0;
  bool will_need_jump = !can_avoid_jump || m_board.randDouble(0, 1) > 0.5;

  // 2.a), 2.b) - choose T_init and x_0
  Length x_0 = last_platform.getActualX() + Length(last_platform.getWidth());
  if (will_need_jump) {
    x_0 = x_0 - Length(GameManager::PLAYER_WIDTH);
  }
  const Time T_init = speedModel.getTime(x_0);

  // 2.a), 2.b) - choose (or infer) T_p
  JumpTrajectory trajectory = JumpTrajectory::randomTrajectory(
      [this](Time min, Time max) { return this->randQuantity(min, max); },
      will_need_jump, T_init, x_0, y_0, Y_p);

  if (will_need_jump) {
    // 4)
    fixCollisions(trajectory);
    Y_p = trajectory.getTargetY();
  }

  // 3)
  const Time T_p = trajectory.getExpectedLandingTime();
  const Length X_p = speedModel.getX(x_0, T_init, T_p);

  double left = m_board.getLocalX(X_p);
  double width = m_board.randDouble(GameManager::TOTAL_HORIZONTAL_DIV * 0.15,
                                    GameManager::TOTAL_HORIZONTAL_DIV * 0.4);

  Platform newPlatform(X_p, left, Y_p.requireRealValue(), width);

  m_board.addPlatform(newPlatform, trajectory);
}