// -*- lsst-c++ -*-

#include "generators/JumpTrajectory.hpp"
#include "GameManager.hpp"
#include "objects/Player.hpp"
#include <stdexcept>
#ifdef _DEBUG
#include <iostream>
#endif

const Speed JumpTrajectory::V_JMP = Speed(-Player::BASE_JUMP_SPEED);
const Acceleration JumpTrajectory::g =
    Acceleration(GameManager::GRAVITY_ACCELERATION);
const Acceleration JumpTrajectory::G_0 =
    Acceleration(-Player::BASE_GLIDE_POWER);

const Time JumpTrajectory::T_max = Time(Player::MAX_GLIDE_DURATION);

JumpTrajectory::JumpTrajectory(bool should_jump, const Time &T_init,
                               const Length &x_0, const Length &y_0,
                               const Time &t_0, const Time &T_p)
    : m_should_jump(should_jump), m_T_init(T_init), m_x_0(x_0), m_y_0(y_0),
      m_t_0(t_0), m_T_p(T_p) {

  if (t_0 > T_max) {
    throw std::logic_error("Illegal gliding time (must be less than T_max)");
  }
  if (should_jump) {
    m_y_gliding =
        Polynomial3<0, 1, 1, 0>(-G_0 / (6 * T_max), (G_0 + g) / 2, V_JMP, y_0);
    m_y_fall = Polynomial2<0, 1, 1, 0>(
        g / 2, V_JMP + G_0 * (t_0 - t_0.square() / (2 * T_max)),
        G_0 * (t_0.cube() / (3 * T_max) - t_0.square() / 2) + y_0);
  } else {
    m_y_fall = Polynomial2<0, 1, 1, 0>(g / 2, Speed(0), y_0);
  }
}

const JumpTrajectory JumpTrajectory::randomTrajectory(
    const RandomProvider &randQuantity, bool should_jump, const Time &T_init,
    const Length &x_0, const Length &y_0, const Length &y) {
  if (should_jump) {
    return forJump(randQuantity, T_init, x_0, y_0, y);
  } else {
    return forFreeFall(T_init, x_0, y_0, y);
  }
}

const Time JumpTrajectory::computeT_1() {
  const Polynomial2<0, 1, 1, -1> derivative(-G_0 / (2 * T_max), G_0 + g, V_JMP);
  const Time root = derivative.positiveRoots().front();
  if (root < T_max) {
    return root;
  }
  return T_max;
}

const Polynomial4<0, 1, 1, 0> JumpTrajectory::JUMP_HEIGHT =
    Polynomial4<0, 1, 1, 0>(
        -G_0.square() / (8 * T_max.square() * g),
        G_0 *(2 * g + 3 * G_0) / (6 * g * T_max),
        G_0 / 2 * ((V_JMP - G_0 * T_max) / (g * T_max) - Scalar(1)),
        -G_0 *V_JMP / g, -V_JMP.square() / (2 * g));

const Time JumpTrajectory::T_1 = computeT_1();

const Length JumpTrajectory::H_MIN = -V_JMP.square() / (2 * g);
const Length JumpTrajectory::H_MAX = JUMP_HEIGHT(T_1);

const Time JumpTrajectory::getFallTime(const Length &y_0, const Length &y) {
  return (2 * (y - y_0) / g).sqrt();
}
const Time JumpTrajectory::getExpectedGlidingTime(const Time &T_p,
                                                  const Length &y_0,
                                                  const Length &y) {
  const Polynomial3<0, 1, 1, 0> y_hat = Polynomial3<0, 1, 1, 0>(
      G_0 / (3 * T_max), -G_0 / 2 * (T_p / T_max + Scalar(1)), G_0 * T_p,
      g * T_p.square() / 2 + V_JMP * T_p + y_0 - y);

  std::vector<Time> roots = y_hat.positiveRoots();
  if (roots.size() > 0) {
    return roots.front();
  }
  return Time(0);
}

const Time JumpTrajectory::getMinJumpLandingTime(const Length &y_0,
                                                 const Length &y) {
  if (y > y_0 + H_MIN) {
    JumpTrajectory trajectory(true, Time(0), Length(0), y_0, Time(0), Time(0));
    return (trajectory.m_y_fall - y).positiveRoots().back();
  } else {
    const Time t_0 = (JUMP_HEIGHT + y_0 - y).positiveRoots().front();
    JumpTrajectory trajectory(true, Time(0), Length(0), y_0, t_0, Time(0));
    return trajectory.getCriticalTime();
  }
}

const Time JumpTrajectory::getMaxJumpLandingTime(const Length &y_0,
                                                 const Length &y) {
  JumpTrajectory trajectory(true, Time(0), Length(0), y_0, T_max, T_max);
  if (T_max < T_1 || y > trajectory.getY(T_max)) {
    return (trajectory.m_y_fall - y).positiveRoots().back();
  } else {
    return (trajectory.m_y_gliding - y).positiveRoots().back();
  }
}

const Time
JumpTrajectory::takeRandomLandingTime(const RandomProvider &randQuantity,
                                      const Length &y_0, const Length &y) {
  Time min_hit_dt = getMinJumpLandingTime(y_0, y);
  Time max_hit_dt = getMaxJumpLandingTime(y_0, y);

  return randQuantity(min_hit_dt, max_hit_dt);
}

const JumpTrajectory
JumpTrajectory::forJump(const RandomProvider &randQuantity, const Time &T_init,
                        const Length &x_0, const Length &y_0, const Length &y) {
  const Time T_p = takeRandomLandingTime(randQuantity, y_0, y);
  const Time t_0 = getExpectedGlidingTime(T_p, y_0, y);

#ifdef _DEBUG
#if 0
    std::cout << "Should jump and glide for " << t_0 << " to reach the platform in " << T_p <<
    "seconds - (y_0 = " << y_0 << "; Y_p = " << y << ")" << std::endl;
#endif
#endif
  return JumpTrajectory(true, T_init, x_0, y_0, t_0, T_p);
}
const JumpTrajectory JumpTrajectory::forFreeFall(const Time &T_init,
                                                 const Length &x_0,
                                                 const Length &y_0,
                                                 const Length &y) {
  return JumpTrajectory(false, T_init, x_0, y_0, Time(0), getFallTime(y_0, y));
}

const Time JumpTrajectory::getCriticalJumpTime(const Time &t_0) {
  const Time new_t_0 = std::min(t_0, T_1);
  return -(V_JMP + G_0 * (new_t_0 - new_t_0.square() / (2 * T_max))) / g;
}

const Time JumpTrajectory::getCriticalTime() const {
  return getCriticalJumpTime(m_t_0);
}

const Length JumpTrajectory::getY(const Time &t) const {
  if (m_should_jump && t < m_t_0) {
    return m_y_gliding(t);
  } else {
    return m_y_fall(t);
  }
}
const Length JumpTrajectory::getTargetY() const { return getY(m_T_p); }

const Length JumpTrajectory::getInitialY() const { return m_y_0; }

const double JumpTrajectory::getY(double t) const {
  return getY(Time(t)).requireRealValue();
}

const Length JumpTrajectory::getX(const ISpeedModel &speedModel,
                                  const Time &t) const {
  return speedModel.getX(m_x_0, m_T_init, t);
}
const long double JumpTrajectory::getX(const ISpeedModel &speedModel,
                                       double t) const {
  return getX(speedModel, Time(t)).requireLongRealValue();
}

const Length JumpTrajectory::getInitialX() const { return m_x_0; }

const Time JumpTrajectory::getBeginningOfEvent() const { return m_T_init; }

void JumpTrajectory::setExpectedLandingTime(const Time &T_p) { m_T_p = T_p; }

const Time &JumpTrajectory::getExpectedGlidingTime() const { return m_t_0; }
const Time &JumpTrajectory::getExpectedLandingTime() const { return m_T_p; }
bool JumpTrajectory::shouldJump() const { return m_should_jump; }
