#include "menu/ContinueGameMenuItem.hpp"
#include "GameManager.hpp"

ContinueGameMenuItem::ContinueGameMenuItem(GameManager &game,
                                           const sf::Font &font)
    : IMenuItem(game, "CONTINUE", font) {}

void ContinueGameMenuItem::reset() {
  IMenuItem::reset();
  setActive(m_game.isAlive());
}

bool ContinueGameMenuItem::onClick() {
  m_game.resumeGame();
  return true;
}