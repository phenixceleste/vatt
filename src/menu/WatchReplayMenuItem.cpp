#include "menu/WatchReplayMenuItem.hpp"
#include "GameManager.hpp"

WatchReplayMenuItem::WatchReplayMenuItem(GameManager &game,
                                         const sf::Font &font)
    : IMenuItem(game, "WATCH REPLAY", font) {}

void WatchReplayMenuItem::reset() {
  IMenuItem::reset();
  setActive(!m_game.isAlive() && std::filesystem::exists("save/replay.save"));
}

bool WatchReplayMenuItem::onClick() {
  m_game.watchReplay();
  return true;
}