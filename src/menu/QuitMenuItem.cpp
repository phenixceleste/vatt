#include "menu/QuitMenuItem.hpp"
#include "GameManager.hpp"

QuitMenuItem::QuitMenuItem(GameManager &game, const sf::Font &font)
    : IMenuItem(game, "QUIT", font) {}

bool QuitMenuItem::onClick() {
  m_game.quitGame();
  return true;
}