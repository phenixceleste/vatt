#include "menu/IMenu.hpp"
#include "GameManager.hpp"

void IMenu::addItem(const std::shared_ptr<IMenuItem> &item) {
  addComponent(item);
}

IMenu::IMenu(GameManager &manager) : m_manager(manager) {}

void IMenu::draw(sf::RenderWindow &window) {
  sf::Vector2u windowSize = window.getSize();
  sf::FloatRect rect(0, 0, windowSize.x, 0);

  for (size_type i = 0; i < getComponentCount(); i++) {
    IMenuItem *item = getItem(i);
    sf::Vector2f size = item->getSize();
    rect.height = size.y + 10;
    item->draw(window, rect);
    rect.top += rect.height;
  }
}
IMenuItem *IMenu::getItem(size_type index) const {
  Component *component_ptr = getComponentList()[index].get();
  IMenuItem *item = reinterpret_cast<IMenuItem *>(component_ptr);
  return item;
}

void IMenu::incrSelection() {
  int selection = (m_selected_index + 1) % getComponentCount();
  while (selection != m_selected_index && !getItem(selection)->isActive()) {
    selection = (selection + 1) % getComponentCount();
  }
  setSelection(selection);
}
void IMenu::decrSelection() {
  int selection =
      (m_selected_index - 1 + getComponentCount()) % getComponentCount();
  while (selection != m_selected_index && !getItem(selection)->isActive()) {
    selection = (selection - 1 + getComponentCount()) % getComponentCount();
  }
  setSelection(selection);
}

void IMenu::setSelection(size_type selection) {
  getItem(m_selected_index)->unselected();
  m_selected_index = selection;
  getItem(m_selected_index)->selected();
  m_manager.invalidateDisplay();
}

void IMenu::reset() {
  bool selected = false;
  for (size_type i = 0; i < getComponentCount(); i++) {
    IMenuItem *item = getItem(i);
    item->reset();
    if (!selected && item->isActive()) {
      setSelection(i);
      selected = true;
    }
  }
}

void IMenu::notifyKeyPressed(const sf::Keyboard::Key &key) {
  switch (key) {
  case sf::Keyboard::Up:
  case sf::Keyboard::Z:
  case sf::Keyboard::W:
    decrSelection();
    break;
  case sf::Keyboard::Down:
  case sf::Keyboard::S:
    incrSelection();
    break;
  case sf::Keyboard::Enter:
    getItem(m_selected_index)->raiseOnClick();
    break;
  default:
    break;
  }
}