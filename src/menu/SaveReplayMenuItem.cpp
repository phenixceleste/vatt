#include "menu/SaveReplayMenuItem.hpp"
#include "GameManager.hpp"

SaveReplayMenuItem::SaveReplayMenuItem(GameManager &game, const sf::Font &font)
    : IMenuItem(game, "SAVE REPLAY -- 10 COINS", font) {}

void SaveReplayMenuItem::reset() {
  IMenuItem::reset();
  setActive(!m_game.isAlive() && m_game.getScore() > 0 &&
            !m_game.isInReplayMode() && m_game.getSession().getCoins() >= 10);
}

bool SaveReplayMenuItem::onClick() {
  if (m_game.getSession().getCoins() >= 10) {
    m_game.saveReplay();
    m_game.resetGame();
    reset();
    m_game.getSession().addCoin(-10);
    return true;
  }

  return false;
}