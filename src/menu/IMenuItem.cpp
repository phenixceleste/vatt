#include "menu/IMenuItem.hpp"
#include "GameManager.hpp"

IMenuItem::IMenuItem(GameManager &game, const std::string &text)
    : m_text(ItemText(text)), m_game(game) {
  reset();
}
IMenuItem::IMenuItem(GameManager &game, const std::string &text,
                     const sf::Font &font)
    : m_text(ItemText(text)), m_game(game) {
  setFont(font);
}

IMenuItem::ItemText::ItemText(const std::string &text) { setString(text); }

void IMenuItem::setFont(const sf::Font &font) { m_text.setFont(font); }
const sf::Font *IMenuItem::getFont() const { return m_text.getFont(); }
void IMenuItem::setText(const std::string &text) {
  m_text.setString(text);
  m_game.invalidateDisplay();
}
const std::string IMenuItem::getText() const { return m_text.getString(); }
void IMenuItem::draw(sf::RenderWindow &window, const sf::FloatRect &clipRect) {
  m_text.setPosition({clipRect.left, clipRect.top});
  sf::Vector2f size = getSize();
  m_text.move({(clipRect.width - size.x) / 2, (clipRect.height - size.y) / 2});
  window.draw(m_text);
}

const sf::Vector2f IMenuItem::getSize() const {
  sf::FloatRect bounds = m_text.getGlobalBounds();
  return sf::Vector2f(bounds.width, bounds.height);
}

void IMenuItem::selected() {
  if (isActive()) {
    m_selected = true;
    m_text.setFillColor(m_selected_color);
    m_game.invalidateDisplay();
  }
}
void IMenuItem::unselected() {
  if (isActive()) {
    m_selected = false;
    m_text.setFillColor(m_color);
    m_game.invalidateDisplay();
  }
}
bool IMenuItem::raiseOnClick() {
  if (isActive()) {
    return onClick();
  }
  return false;
}
void IMenuItem::setActive(bool active) {
  Component::setActive(active);
  if (active) {
    if (m_selected) {
      m_text.setFillColor(m_selected_color);
    } else {
      m_text.setFillColor(m_color);
    }
  } else {
    m_text.setFillColor(m_inactive_color);
  }
  m_game.invalidateDisplay();
}

void IMenuItem::reset() {
  unselected();
  setActive(true); // to update color
  m_game.invalidateDisplay();
}

bool IMenuItem::onClick() {
  // onClick cannot be virtual pure otherwise the compiler complains
  // when IMenuItem is used as a generic base type...
  throw std::logic_error("Not implemented");
}