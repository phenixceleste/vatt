#include "menu/GameMenu.hpp"

GameMenu::GameMenu(GameManager &manager, const sf::Font &font)
    : IMenu(manager) {

  addItem(std::make_shared<ContinueGameMenuItem>(manager, font));
  addItem(std::make_shared<NewGameMenuItem>(manager, font));
  addItem(std::make_shared<SaveReplayMenuItem>(manager, font));
  addItem(std::make_shared<WatchReplayMenuItem>(manager, font));
  addItem(std::make_shared<QuitMenuItem>(manager, font));

  reset();
}
