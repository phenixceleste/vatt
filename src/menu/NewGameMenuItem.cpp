#include "menu/NewGameMenuItem.hpp"
#include "GameManager.hpp"

NewGameMenuItem::NewGameMenuItem(GameManager &game, const sf::Font &font)
    : IMenuItem(game, "NEW GAME", font) {}

bool NewGameMenuItem::onClick() {
  if (m_game.isAlive() || m_game.getScore() > 0) {
    m_game.resetGame();
  }
  m_game.startGame(false);
  return true;
}