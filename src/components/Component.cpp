#include "components/Component.hpp"

bool Component::isActive() { return m_is_active; }

void Component::setActive(bool active) { m_is_active = active; }
