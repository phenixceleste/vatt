#include "components/GraphicComponent.hpp"
#include <stdexcept>

GraphicComponent::GraphicComponent() {}

void GraphicComponent::nextFrame() {
  m_frame_count++;
  if (m_frame_count >= m_frame_per_texture) {
    m_frame_count = 0;
    m_current_animation_frame++;
    m_current_animation_frame %= m_paths.size();
  }
}

const sf::Texture &GraphicComponent::getTexture() {
  if (m_paths.size() == 0) {
    throw std::logic_error("No texture set.");
  }
  const filesystem_path path = m_paths[m_current_animation_frame];
  load(path);
  return m_textures[path];
}

void GraphicComponent::setTexture(const filesystem_path &path) {
  load(path);
  m_paths.clear();
  m_paths.push_back(path);

  m_frame_per_texture = 1;
  m_current_animation_frame = 0;
  m_frame_count = 0;
}

void GraphicComponent::animate(const std::vector<filesystem_path> &paths,
                               unsigned int framesPerTexture) {
  if (framesPerTexture == 0) {
    throw std::logic_error("framesPerTexture must be strictly greater than 0.");
  }

  load(paths);
  m_paths.clear();
  m_paths = std::move(paths);

  m_frame_per_texture = framesPerTexture;
  m_current_animation_frame = 0;
  m_frame_count = 0;
}

void GraphicComponent::setAnimationTexture(unsigned int id) {
  m_current_animation_frame = id % m_paths.size();
  m_frame_count = 0;
}

void GraphicComponent::load(const filesystem_path &path) {
  if (!isLoaded(path)) {
    // Will create the mapping if needed.
    m_textures[path].loadFromFile(path);
  }
}

void GraphicComponent::load(const std::vector<filesystem_path> &paths) {
  for (const filesystem_path &path : paths) {
    load(path);
  }
}

bool GraphicComponent::isLoaded(const filesystem_path &path) const {
  return m_textures.find(path) != m_textures.end();
}