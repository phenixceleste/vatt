#include "components/SpeedModel.hpp"
#include "GameManager.hpp"
#include <cmath>

HorizontalSpeedModel::HorizontalSpeedModel(const Speed &v_0, double k,
                                           double alpha)
    : m_v_0(v_0), m_k(k), m_alpha(alpha) {}

const Time HorizontalSpeedModel::T_ref = Time(1);

const Length HorizontalSpeedModel::getXCoeff() const {
  return m_v_0 * m_k * T_ref / (m_alpha + 1);
}
const Scalar HorizontalSpeedModel::getScalarToRaise(const Time &t) const {
  return Scalar(t / (m_k * T_ref)) + 1;
}

const Speed HorizontalSpeedModel::getSpeed(const Time &t) const {
  return m_v_0 * getScalarToRaise(t).pow(m_alpha);
}

const Length HorizontalSpeedModel::getX(const Time &t) const {
  return getXCoeff() * (getScalarToRaise(t).pow(m_alpha + 1) - 1);
}
const Length HorizontalSpeedModel::getX(const Length &x_0, const Time &T_init,
                                        const Time &t) const {
  return x_0 + getXCoeff() * (getScalarToRaise(t + T_init).pow(m_alpha + 1) -
                              getScalarToRaise(T_init).pow(m_alpha + 1));
}

const Time HorizontalSpeedModel::getTime(const Length &x) const {
  return m_k * T_ref *
         (Scalar(Scalar((m_alpha + 1) * x / (m_v_0 * m_k * T_ref)) + 1)
              .pow(1 / (m_alpha + 1)) -
          1);
}