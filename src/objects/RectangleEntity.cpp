// -*- lsst-c++ -*-

#include "objects/RectangleEntity.hpp"
#include "GameManager.hpp"

RectangleEntity::RectangleEntity(double x, double y, double width,
                                 double height)
    : m_x(x), m_y(y), m_width(width), m_height(height) {}

bool RectangleEntity::isColliding(const RectangleEntity &other) const {
  return m_x + m_width >= other.getX() && m_y + m_height >= other.getY() &&
         other.getX() + other.getWidth() >= m_x &&
         other.getY() + other.getHeight() >= m_y;
}

void RectangleEntity::move(double x, double y) {
  m_x += x;
  m_y += y;
}

double RectangleEntity::getX() const { return m_x; }

void RectangleEntity::setX(double x) { m_x = x; }

double RectangleEntity::getY() const { return m_y; }

void RectangleEntity::setY(double y) { m_y = y; }

double RectangleEntity::getWidth() const { return m_width; }

void RectangleEntity::setWidth(double width) { m_width = width; }

double RectangleEntity::getHeight() const { return m_height; }

void RectangleEntity::setHeight(double height) { m_height = height; }

double RectangleEntity::getRight() const { return m_x + m_width; }

double RectangleEntity::getBottom() const { return m_y + m_height; }

const double epsilon = 1e-5;

bool RectangleEntity::isJustAbove(const PlaneEntity &other) const {
  return m_y < other.getY() - m_height &&
         m_y >= other.getY() - m_height - epsilon;
}

void RectangleEntity::putJustAbove(const PlaneEntity &other) {
  m_y = other.getY() - m_height - epsilon;
}
void RectangleEntity::putJustBelow(const PlaneEntity &other) {
  m_y = other.getY() + epsilon;
}

const sf::RectangleShape
RectangleEntity::getShape(const sf::RenderWindow &window) {
  sf::Vector2d scale = getScaleVector(window);
  sf::RectangleShape rectangle;
  rectangle.setPosition(scale.x * m_x, scale.y * m_y);
  rectangle.setSize({(float)(scale.x * m_width), (float)(scale.y * m_height)});
  specializeRectangleShape(rectangle);

  return rectangle;
}
