#include "objects/GameObject.hpp"
#include <memory>
#include <vector>

const std::vector<std::shared_ptr<Component>>
GameObject::getComponentList() const {
  return m_components;
}

void GameObject::addComponent(std::shared_ptr<Component> new_component) {
  m_components.push_back(new_component);
}

const typename std::vector<std::shared_ptr<Component>>::size_type
GameObject::getComponentCount() const {
  return m_components.size();
}