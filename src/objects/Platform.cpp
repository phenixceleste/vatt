// -*- lsst-c++ -*-

#include "objects/Platform.hpp"
#include "objects/Player.hpp"
#include <cmath>
#ifdef _DEBUG
#include <iostream>
#endif

Platform::Platform(const Length &actual_X_p, double x, double y, double width)
    : PlaneEntity(x, y, width), m_actual_X_p(actual_X_p) {}

void Platform::specializeConvexShape(sf::ConvexShape &shape) {
  shape.setOutlineColor(sf::Color(220, 220, 220));
}

void Platform::setJumpTrajectory(const JumpTrajectory &jumpTrajectory) {
  m_jump_trajectory = std::optional<JumpTrajectory>(jumpTrajectory);
}
bool Platform::hasJumpTrajectory() const { return (bool)m_jump_trajectory; }
const JumpTrajectory &Platform::getJumpTrajectory() const {
  return *m_jump_trajectory;
}

const Length &Platform::getActualX() const { return m_actual_X_p; }