// -*- lsst-c++ -*-

#include "objects/Player.hpp"

std::vector<filesystem_path> Player::animation_frames;

void Player::loadAssets() {
  animation_frames.clear();
  animation_frames.push_back("assets/player-1.png");
  animation_frames.push_back("assets/player-2.png");
  animation_frames.push_back("assets/player-3.png");
  animation_frames.push_back("assets/player-4.png");
  animation_frames.push_back("assets/player-5.png");
}

Player::Player(double x, double y, double width, double height,
               GameManager &board)
    : RectangleEntity(x, y, width, height), m_base_height(height), m_speed_y(0),
      m_board(board) {
  updateTexture();
}

void Player::startJump() noexcept {
  if (m_rolling) {
    return;
  }
  OptionalRef<const Platform &> _;
  if (m_board.isOnAPlatform(*this, _)) {
    m_gliding = BASE_GLIDE_POWER;
    m_speed_y = -BASE_JUMP_SPEED;
  }
  // Cannot jump if the player is not on the ground.
}

void Player::stopJump() noexcept { m_gliding = 0; }

void Player::fall(double new_y) {

  OptionalRef<const Platform &> platform;
  if (m_board.isOnAPlatform(*this, platform) && m_speed_y > 0) {
    if (m_rolling && !m_bent_down) {
      setHeight(m_base_height * BENT_HEIGHT_FACTOR);
      putJustAbove(*platform);
      m_bent_down = true;
      updateTexture();
    } else if (!m_rolling && m_bent_down) {
      setHeight(m_base_height);
      putJustAbove(*platform);
      m_bent_down = false;
      updateTexture();
    }
    m_speed_y = 0;
    if (!m_is_on_a_platform) {
      m_is_on_a_platform = true;
      updateTexture();
    }

    return;
  } else if (m_is_on_a_platform) {
    m_is_on_a_platform = false;
    updateTexture();
  }

  double old_y = getY();
  setY(new_y);

  if (m_board.isCollidingPlatform(*this, platform)) {
    if (platform->getY() <= old_y) {
      // Below the platform, cannot go through it.
      putJustBelow(*platform);
    } else if (platform->getY() >= old_y) {
      // The player is on a platform again
      putJustAbove(*platform);
      m_is_on_a_platform = true;
      updateTexture();
    }
    stopJump();
    m_speed_y = 0;
  }
}

bool Player::tick(double elapsed) {

  double acceleration = GameManager::GRAVITY_ACCELERATION;

  if (m_gliding > 0) {
    acceleration -= m_gliding;
    m_gliding -= BASE_GLIDE_POWER * elapsed / MAX_GLIDE_DURATION;
  }
  m_speed_y += acceleration * elapsed;

  fall(getY() + m_speed_y * elapsed);

  m_graphic_component.nextFrame();

  return getY() <= GameManager::TOTAL_VERTICAL_DIV;
}

void Player::startRoll() {
  if (m_gliding > 0) {
    return;
  }
  m_rolling = true;
}

void Player::stopRoll() { m_rolling = false; }

void Player::collect(Coin &coin) {
  coin.markCollected();
  if (!m_board.isInReplayMode()) {
    m_board.getSession().addCoin(1);
  }
}

int Player::getPlayerId() const { return m_player_id; }

void Player::specializeRectangleShape(sf::RectangleShape &shape) {
  const sf::Texture *texture = &m_graphic_component.getTexture();
  shape.setTexture(texture);
}

void Player::updateTexture() {
  if (m_bent_down) {
    m_graphic_component.setTexture("assets/player-bent.png");
  } else if (m_is_on_a_platform) {
    m_graphic_component.animate(animation_frames,
                                Player::ANIMATION_FRAMES_DURATION);
  } else {
    m_graphic_component.setTexture("assets/player-jumping.png");
  }
}