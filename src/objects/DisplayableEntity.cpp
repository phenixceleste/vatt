#include "objects/DisplayableEntity.hpp"
#include "GameManager.hpp"

DisplayableEntity::DisplayableEntity() {}

const sf::Vector2d
DisplayableEntity::getScaleVector(const sf::RenderWindow &window) const {
  sf::Vector2u windowSize = window.getSize();
  double scale_X = (double)windowSize.x / GameManager::TOTAL_HORIZONTAL_DIV;
  double scale_Y = (double)windowSize.y / GameManager::TOTAL_VERTICAL_DIV;

  return {scale_X, scale_Y};
}