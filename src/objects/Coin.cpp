// -*- lsst-c++ -*-

#include "objects/Coin.hpp"

Coin::Coin(double x, double y)
    : CircleEntity(x, y, Coin::COIN_RADIUS, Coin::COIN_NUMBER_OF_POINTS) {}

void Coin::specializeCircleShape(sf::CircleShape &shape) {
  shape.setOutlineColor(sf::Color(220, 220, 0, 255));
  shape.setFillColor(sf::Color(220, 220, 0, 255));
  shape.setOutlineThickness(2);
}