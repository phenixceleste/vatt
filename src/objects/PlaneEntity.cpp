// -*- lsst-c++ -*-

#include "objects/PlaneEntity.hpp"
#include "GameManager.hpp"

bool PlaneEntity::isColliding(const RectangleEntity &other) const {
  return m_x + m_width >= other.getX() && m_y >= other.getY() &&
         other.getX() + other.getWidth() >= m_x &&
         other.getY() + other.getHeight() >= m_y;
}

void PlaneEntity::move(double x, double y) {
  m_x += x;
  m_y += y;
}

double PlaneEntity::getX() const { return m_x; }

void PlaneEntity::setX(double x) { m_x = x; }

double PlaneEntity::getY() const { return m_y; }

void PlaneEntity::setY(double y) { m_y = y; }

double PlaneEntity::getWidth() const { return m_width; }

void PlaneEntity::setWidth(double width) { m_width = width; }

double PlaneEntity::getRight() const { return m_x + m_width; }

const sf::ConvexShape PlaneEntity::getShape(const sf::RenderWindow &window) {
  sf::Vector2d scale = getScaleVector(window);
  sf::ConvexShape line;
  line.setPointCount(4);
  line.setPoint(0, sf::Vector2f(0, 0));
  line.setPoint(1, sf::Vector2f(scale.x * m_width, 0));
  line.setPoint(2, sf::Vector2f(scale.x * m_width, 0));
  line.setOutlineThickness(1);
  line.setPosition(scale.x * m_x, scale.y * m_y);
  specializeConvexShape(line);

  return line;
}
