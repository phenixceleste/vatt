// -*- lsst-c++ -*-

#include "objects/CircleEntity.hpp"
#include "GameManager.hpp"

bool CircleEntity::isColliding(const RectangleEntity &other) const {
  double r_x = other.getX();
  double r_y = other.getY();
  double r_w = other.getWidth();
  double r_h = other.getHeight();

  if (m_x < r_x) {
    if (m_y < r_y) {
      return pow(m_radius, 2) >= pow(m_x - r_x, 2) + pow(m_y - r_y, 2);
    } else if (m_y > r_y + r_h) {
      return pow(m_radius, 2) >= pow(m_x - r_x, 2) + pow(m_y - r_y - r_h, 2);
    } else {
      return m_radius >= r_x - m_x;
    }
  } else if (m_x > r_x + r_w) {
    if (m_y < r_y) {
      return pow(m_radius, 2) >= pow(m_x - r_x - r_w, 2) + pow(m_y - r_y, 2);
    } else if (m_y > r_y + r_h) {
      return pow(m_radius, 2) >=
             pow(m_x - r_x - r_w, 2) + pow(m_y - r_y - r_h, 2);
    } else {
      return m_radius >= m_x - r_x - r_w;
    }
  } else {
    if (m_y < r_y) {
      return m_radius >= r_y - m_y;
    } else if (m_y > r_y + r_h) {
      return m_radius >= m_y - r_y - r_h;
    } else {
      return true;
    }
  }
}

void CircleEntity::move(double x, double y) {
  m_x += x;
  m_y += y;
}

double CircleEntity::getX() const { return m_x; }

void CircleEntity::setX(double x) { m_x = x; }

double CircleEntity::getY() const { return m_y; }

void CircleEntity::setY(double y) { m_y = y; }

double CircleEntity::getRadius() const { return m_radius; }

void CircleEntity::setRadius(double radius) { m_radius = radius; }

double CircleEntity::getRight() const { return m_x + m_radius; }

const sf::CircleShape CircleEntity::getShape(const sf::RenderWindow &window) {
  const sf::Vector2d scale = getScaleVector(window);
  sf::CircleShape circle;
  circle.setRadius(m_radius);
  circle.setScale({(float)scale.x, (float)scale.y});
  circle.setPointCount(m_point_count);
  circle.setPosition(scale.x * m_x, scale.y * m_y);
  specializeCircleShape(circle);

  return circle;
}
