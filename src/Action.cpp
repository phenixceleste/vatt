#include "Action.hpp"

Action::Action(bool jump, bool press, double time)
    : m_is_jump(jump), m_is_press(press), m_time(time) {}

Action Action::fromString(std::string s) {
  bool is_jump = s[0] == '1';
  bool is_press = s[1] == '1';
  double time = std::stod(s.substr(2, s.size()));
  return Action(is_jump, is_press, time);
}

std::string Action::toString() {
  return std::string(m_is_jump ? "1" : "0") +
         std::string(m_is_press ? "1" : "0") + std::to_string(m_time);
}

void Action::saveReplayList(std::vector<Action> all_actions,
                            unsigned int seed) {
  std::vector<std::string> stringVect;
  for (int i = 0; i < (int)all_actions.size(); i++) {
    stringVect.push_back(all_actions[i].toString());
  }
  std::ofstream file("save/replay.save");
  file << std::to_string(seed) << std::endl;
  file << linefeedFold(stringVect);
  file.close();
}

std::vector<Action> Action::loadReplay(unsigned int *seed) {
  if (std::filesystem::exists("save/replay.save")) {
    std::ifstream file("save/replay.save");
    if (file.is_open()) {
      std::string line;
      std::vector<Action> res;

      getline(file, line);
      *seed = std::stoul(line);

      while (getline(file, line)) {
        res.push_back(Action::fromString(line));
      }

      file.close();
      return res;
    }
    return {};
  }
  return {};
}
