#include "util.hpp"

std::string linefeedFold(std::vector<std::string> stringVect) {
  std::string res;
  for (int i = 0; i < (int)stringVect.size(); i++) {
    res += stringVect[i] + "\n";
  }
  return res;
}