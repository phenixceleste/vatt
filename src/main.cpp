// -*- lsst-c++ -*-

#include "GameManager.hpp"
#include "GameWindow.hpp"
#include "Session.hpp"
#include <SFML/Graphics.hpp>

#ifdef _TEST
#include "tests/CommandLineParser.hpp"
#include "tests/TestJump.hpp"
#include "tests/TestPlatformGeneration.hpp"
#include <iostream>

int main(int argc, char *argv[])
#else
int main()
#endif
{
  GameManager::loadAssets();
  GameWindow window;

  JumpTrajectory t(false, Time(0), Length(0), Length(0), Time(0), Time(0));

#ifdef _TEST
  bool test_run = false;
  bool test_success = true;

  CommandLineParser parser;
  parser.parse(argc, argv);
  bool fast_mode = parser.isSet("--fast");

  if (parser.isSet("jump")) {
    test_run = true;
    TestJump test;
    test_success = test.runTest(window, fast_mode) && test_success;
  }
  if (parser.isSet("plat_gen")) {
    test_run = true;
    TestPlatformGeneration test;
    test_success =
        test.runTest(window, fast_mode, parser.optValue("plat_gen", -1)) &&
        test_success;
  }

  if (!test_run) {
    std::cout << "No test specified. Usage: ./vatt_test <test1> [test2] ..."
              << std::endl;
  }

  window.close();
  return (test_run && test_success) ? 0 : 1;

#else
  std::filesystem::create_directory("save");
  Session session = Session::fromFile("save/test.lfsv");
  GameManager board(session);
  board.attachWindow(window);
  board.launchManager();
  board.detachWindow();
  window.close();
  return 0;
#endif
}
