#include "TimeManager.hpp"
#include <chrono>
#include <stdexcept>

typedef std::chrono::system_clock __clock;
const auto __clock_period = __clock::period::den;
typedef std::chrono::duration<int64_t, __clock::period> __duration;

__duration durationFromDoubleSeconds(double seconds) {
  return __duration((int)(seconds * __clock_period));
}

TimePoint::TimePoint(OptionalRef<TimeManager &> &parent,
                     const __clock::time_point &time)
    : m_parent(parent), m_time(time) {}

TimePoint::TimePoint() {}

void throw_uninitialized() {
  throw std::logic_error(
      "TimePoint: Illegal usage attempt of an uninitialized time point");
}

unsigned int TimePoint::time_since_epoch() const {
  if (!m_parent) {
    throw_uninitialized();
  }
  auto tse = m_time.time_since_epoch();
  return (unsigned int)std::chrono::duration_cast<std::chrono::seconds>(tse)
      .count();
}

TimeManager::TimeManager(double speed) : m_scale(speed) {
  if (speed <= 0) {
    throw std::logic_error("TimeManager: Speed must be strictly positive.");
  }
}

const TimePoint TimeManager::getCurrentTime() {
  OptionalRef<TimeManager &> parent(*this);
  return TimePoint(parent, __clock::now());
}

double TimePoint::operator-(const TimePoint &rhs) const {
  if (!m_parent || !rhs.m_parent) {
    throw_uninitialized();
  }
  if (&*m_parent != &*rhs.m_parent) {
    throw std::logic_error("Incomparables time points");
  }

  const __duration duration = m_time - rhs.m_time;
  return m_parent->m_scale * (double)duration.count() / (double)__clock_period;
}

const TimePoint TimePoint::operator+(double time) const {
  OptionalRef<TimeManager &> parent(m_parent);
  return TimePoint(parent, m_time + durationFromDoubleSeconds(time));
}

TimePoint &TimePoint::operator+=(double time) {
  this->m_time += durationFromDoubleSeconds(time);
  return *this;
}