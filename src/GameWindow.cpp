// -*- lsst-c++ -*-

#include "GameWindow.hpp"
#include <stdexcept>
#include <unistd.h>

GameWindow::GameWindow() {
  m_settings.antialiasingLevel = 8.0;
  toggleFullScreen();
  this->setKeyRepeatEnabled(false);
}

void GameWindow::toggleFullScreen() {
  m_fullscreen = !m_fullscreen;
  if (m_fullscreen) {
    std::vector<sf::VideoMode> supportedModes =
        sf::VideoMode::getFullscreenModes();
    if (supportedModes.empty()) {
      m_fullscreen = false;
      return;
    }
    create(supportedModes.front(), TITLE, sf::Style::Fullscreen, m_settings);
  } else {
    sf::VideoMode screen = sf::VideoMode::getDesktopMode();
    sf::VideoMode windowSize(screen.width * 0.9, screen.height * 0.9);

    create(windowSize, TITLE, sf::Style::Default, m_settings);

    sf::Vector2i position(screen.width * 0.05, screen.height * 0.02);
    setPosition(position);
  }
}