// -*- lsst-c++ -*-

#include "GameManager.hpp"
#include <cmath>
#ifdef _DEBUG
#include <iostream>
#endif
#include <iostream>
#include <stdexcept>
#include <time.h>

sf::Font GameManager::font;

void GameManager::loadAssets() {
  GameManager::font.loadFromFile("assets/arial.ttf");
  Player::loadAssets();
}

GameManager::GameState::GameState(GameManager &manager,
                                  unsigned int seed /* = 0 */,
                                  unsigned int num_players /*= 1*/) {

  if (num_players == 0) {
    throw std::logic_error("GameManager: There must be at least one player");
  }
  if (num_players != 1) {
    throw std::logic_error("GameManager: Multiplayer not yet implemented");
  }

  if (seed == 0) {
    seed = manager.getTimeManager().getCurrentTime().time_since_epoch();
  }

  m_random_generator.seed(seed);
  m_seed = seed;

  const int width = GameManager::TOTAL_HORIZONTAL_DIV;
  const int height = GameManager::TOTAL_VERTICAL_DIV;

  const double player_x = width / 20.0;
  const double player_y = 0;

  for (unsigned int i = 0; i < num_players; i++) {
    m_players.push_back(
        Player(player_x, player_y, PLAYER_WIDTH, PLAYER_HEIGHT, manager));
  }

  // Start with an initial platform
  Platform plat(Length(-m_players[0].getX()), 0, height * 0.7, width / 3.0);
  m_players[0].putJustAbove(plat);
  m_platforms.push_back(plat);

  manager.generateMap();
}

GameManager::GameState &GameManager::GameState::operator=(GameState &&state) {
  m_seed = state.m_seed;
  m_score = state.m_score;
  m_alive = state.m_alive;
  m_running = state.m_running;
  m_time = state.m_time;
#ifdef _TEST
  m_player_running = state.m_player_running;
#endif
  m_players = std::move(state.m_players);
  m_platforms = std::move(state.m_platforms);
  m_coins = std::move(state.m_coins);
  m_all_actions = std::move(state.m_all_actions);
  m_random_generator = state.m_random_generator;
  m_real_distrib = state.m_real_distrib;
  return *this;
}

GameManager::GameManager(Session &session, unsigned int seed /* = 0 */,
                         unsigned int num_players /*= 1*/)
    : m_session(session),
      m_speed_model(Speed(TOTAL_HORIZONTAL_DIV / 10.0), 7.0, 0.5),
      m_map_generator(*this), m_state(*this, seed, num_players),
      m_menu(GameMenu(*this, font)) {
  m_last_tick = m_time_manager.getCurrentTime();
}

GameManager::~GameManager() {
  detachWindow();
  m_session.save();
}

double GameManager::getGameSpeed() const {
#ifdef _TEST
  if (!m_state.m_player_running) {
    return 0;
  }
#endif
  return m_speed_model.getSpeed(Time(getElapsedTime())).requireRealValue();
}

const ISpeedModel &GameManager::getGameSpeedModel() const {
  return m_speed_model;
}

double GameManager::getLocalX(const Length &x) const {
  const Length x_0 = m_speed_model.getX(Time(getElapsedTime()));
  return (x - x_0).requireRealValue() + m_state.m_players[0].getX();
}

double GameManager::getScore() const { return m_state.m_score; }

double GameManager::getElapsedTime() const { return m_state.m_time; }

bool GameManager::isOnAPlatform(const Player &player,
                                OptionalRef<const Platform &> &ret_platform) {
  for (const Platform &platform : m_state.m_platforms) {
    if (platform.getX() <= player.getRight() &&
        platform.getRight() >= player.getX() && player.isJustAbove(platform)) {
      ret_platform.inplace(platform);
      return true;
    }
  }
  return false;
}

bool GameManager::isCollidingPlatform(
    const Player &player, OptionalRef<const Platform &> &ret_platform) {
  for (const Platform &platform : m_state.m_platforms) {
    if (platform.isColliding(player)) {
      ret_platform.inplace(platform);
      return true;
    }
  }
  return false;
}

bool GameManager::update(double elapsed) {
  double speed = elapsed * getGameSpeed();

  bool alive = true;

  m_state.m_time += elapsed;
  m_state.m_score += elapsed;
  if (m_state.m_replaying) {
    double time = getElapsedTime();
    if (m_state.m_all_actions.size() > 0 &&
        time >= m_state.m_all_actions[0].m_time) {
      executeAction(m_state.m_all_actions[0]);
      m_state.m_all_actions.erase(m_state.m_all_actions.begin());
    }
  }

  for (Platform &platform : m_state.m_platforms) {
    platform.move(-speed, 0);
    for (Player &player : m_state.m_players) {
      if (platform.isColliding(player)) {
#if _DEBUG
        std::cout << "Player " << player
                  << " died after a collision with: " << platform << std::endl;
#endif
        alive = false;
      }
    }
  }

  for (Coin &coin : m_state.m_coins) {
    coin.move(-speed, 0);

    for (Player &player : m_state.m_players) {
      if (coin.isColliding(player) && !coin.isCollected()) {
        player.collect(coin);
      }
    }
  }

  m_state.m_platforms.erase(
      std::remove_if(m_state.m_platforms.begin(), m_state.m_platforms.end(),
                     [](Platform &platform) {
                       return platform.getRight() <
                              -GameManager::TOTAL_HORIZONTAL_DIV;
                     }),
      m_state.m_platforms.end());

  m_state.m_coins.erase(
      std::remove_if(m_state.m_coins.begin(), m_state.m_coins.end(),
                     [](Coin &coin) {
                       return coin.getRight() <
                                  -GameManager::TOTAL_HORIZONTAL_DIV ||
                              coin.isCollected();
                     }),
      m_state.m_coins.end());

  for (Player &player : m_state.m_players) {
    alive = player.tick(elapsed) && alive;
  }

  return alive;
}

void GameManager::tick() {

#ifdef _TEST
  double elapsed;
  if (m_fast_mode) {
    elapsed = TICK_INTERVAL;
  } else {
    TimePoint current = m_time_manager.getCurrentTime();
    elapsed = current - m_last_tick;
  }
#else
  TimePoint current = m_time_manager.getCurrentTime();
  double elapsed = current - m_last_tick;
#endif

  while (elapsed >= TICK_INTERVAL) {
    // Running a tick (or multiple ticks if some ticks have been missed)

    if (isRunning() || (isAlive() && m_state.m_tick_pending)) {
      if (update(TICK_INTERVAL)) {
        generateMap();
      } else {
        m_state.m_alive = false;
        onGameStop();
      }
      onTick();
      invalidateDisplay();
      m_state.m_tick_pending = false;
    }

    render();

    m_last_tick += TICK_INTERVAL;
    elapsed -= TICK_INTERVAL;
  }

#ifdef _TEST
  if (!m_fast_mode) {
#endif
    double sleep_time =
        (TICK_INTERVAL - (m_time_manager.getCurrentTime() - m_last_tick));
    if (sleep_time > 0) {
      sf::sleep(sf::milliseconds((float)(5e2 * sleep_time)));
    }
#ifdef _TEST
  }
#endif
}

void GameManager::onTick() {
  for (const OnTickListener &listener : m_tick_listeners) {
    listener(*this);
  }
}

Player &GameManager::getPlayer(int id) { return m_state.m_players[id]; }

Session &GameManager::getSession() { return m_session; }

const Platform &GameManager::getLastPlatform() {
  return m_state.m_platforms.back();
}
const JumpTrajectory &GameManager::getLastJumpTrajectory() {
  if (m_state.m_platforms.size() > 1) {
    return m_state.m_platforms[m_state.m_platforms.size() - 2]
        .getJumpTrajectory();
  }
  throw std::logic_error("No trajectory yet.");
}

void GameManager::addPlatform(const Platform &newPlatform,
                              const JumpTrajectory &jumpTrajectory) {
  Platform &plat = m_state.m_platforms.back();
  plat.setJumpTrajectory(jumpTrajectory);
  m_state.m_platforms.push_back(newPlatform);
}
void GameManager::addCoin(const Coin &coin) { m_state.m_coins.push_back(coin); }

void GameManager::drawAlive() {

  for (Platform &platform : m_state.m_platforms) {
    platform.draw(*m_window);
  }
  for (Player &player : m_state.m_players) {
    player.draw(*m_window);
  }

  for (Coin &coin : m_state.m_coins) {
    if (!coin.isCollected()) {
      coin.draw(*m_window);
    }
  }

  sf::Text text;
  text.setFont(GameManager::font);
  text.setString("Score: " + std::to_string((int)getScore()));
  text.setFillColor(sf::Color::White);
  text.setCharacterSize(24);
  text.setStyle(sf::Text::Bold);
  text.setPosition(
      {(float)((m_window->getSize().x - text.getGlobalBounds().width - 24)),
       20});
  m_window->draw(text);

  if (!isInReplayMode()) {
    text.setFont(GameManager::font);
    text.setString("Best Score: " +
                   std::to_string((int)getSession().getBestScore()));
    text.setFillColor(sf::Color::White);
    text.setCharacterSize(24);
    text.setStyle(sf::Text::Bold);
    text.setPosition(
        {(float)((m_window->getSize().x - text.getLocalBounds().width - 24)),
         44});
    m_window->draw(text);

    text.setFont(GameManager::font);
    text.setString("Coins: " + std::to_string((int)getSession().getCoins()));
    text.setFillColor(sf::Color::White);
    text.setCharacterSize(24);
    text.setStyle(sf::Text::Bold);
    text.setPosition(
        {(float)((m_window->getSize().x - text.getGlobalBounds().width - 24)),
         68});
    m_window->draw(text);
  }
}
void GameManager::drawMenu() { m_menu.draw(*m_window); }
void GameManager::draw() {

  m_window->clear(sf::Color(20, 20, 60, 255));

  if (isRunning()) {
    drawAlive();
  } else {
    drawMenu();
  }
  m_need_redraw = false;
}

void GameManager::render() {
  if (m_window) {
    if (shouldRedraw()) {
      draw();
      m_window->display();
    }
    sf::Event event;
    while (m_window && m_window->pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        m_window->close();
        quitGame();
      } else if (m_input_enabled) {
        notifyEvent(event);
      }
    }
  }
}

bool GameManager::shouldRedraw() { return m_need_redraw; }
void GameManager::invalidateDisplay() { m_need_redraw = true; }

double GameManager::randDouble(double min, double max) {
  if (min == max) {
    return min;
  }
  if (min > max) {
    throw std::logic_error(
        "GameManager::randDouble: Min must be lower than max.");
  }

  return m_state.m_real_distrib(
      m_state.m_random_generator,
      std::uniform_real_distribution<>::param_type(min, max));
}

bool GameManager::needsNewPlatform() {
  return m_state.m_platforms.back().getX() <=
         2 * GameManager::TOTAL_HORIZONTAL_DIV;
}

void GameManager::generateMap() {
  while (needsNewPlatform()) {
    m_map_generator.generateMap();
  }
}

void GameManager::manager_loop() {

  while (m_launched) {
    tick();
  }
  onGameQuit();
}

void GameManager::onGameStop() {
  enableInput();
  m_menu.reset();
  for (OnGameOverListener &listener : m_game_over_listeners) {
    listener(*this);
  }
}
void GameManager::onGameQuit() {
  m_launched = false;
  for (OnGameQuitListener &listener : m_game_quit_listeners) {
    listener(*this);
  }
}

void GameManager::startGame(bool replaying) {

  m_state.m_replaying = replaying;

  if (!m_launched) {
    throw std::logic_error("Manager not launched");
  }

  if (isAlive()) {
    return;
  }

  m_last_tick = m_time_manager.getCurrentTime();
  m_state.m_alive = true;
  m_state.m_running = true;
  invalidateDisplay();
}

void GameManager::stopGame() noexcept {
  m_state.m_alive = false;
  m_session.updateBestScore(getScore());

  enableInput();

  m_menu.reset();
}

void GameManager::pauseGame() noexcept {
  m_state.m_running = false;
  m_state.m_tick_pending = true;
  getPlayer(0).stopRoll();
  m_menu.reset();
}
void GameManager::resumeGame() noexcept {
  m_last_tick = m_time_manager.getCurrentTime();
  m_state.m_running = true;
  invalidateDisplay();
}
void GameManager::pauseOrResumeGame() noexcept {
  if (!isAlive()) {
    return;
  }
  if (m_state.m_running) {
    pauseGame();
  } else {
    resumeGame();
  }
}

bool GameManager::isAlive() { return m_launched && m_state.m_alive; }

bool GameManager::isRunning() { return isAlive() && m_state.m_running; }

void GameManager::notifyKeyPressed(const sf::Keyboard::Key &key) {
  if (key == sf::Keyboard::Escape) {
    pauseOrResumeGame();
    return;
  } else if (m_window && key == sf::Keyboard::F11) {
    bool shouldResume = false;
    if (isRunning()) {
      shouldResume = true;
      pauseGame();
    }
    m_window->toggleFullScreen();
    invalidateDisplay();
    if (shouldResume) {
      resumeGame();
    }
  }
  if (!isRunning()) {
    m_menu.notifyKeyPressed(key);
    return;
  }
  switch (key) {
  case sf::Keyboard::Space:
    getPlayer(0).startJump();
    m_state.m_all_actions.push_back(Action(true, true, getElapsedTime()));
    break;
  case sf::Keyboard::Down:
  case sf::Keyboard::S:
    getPlayer(0).startRoll();
    m_state.m_all_actions.push_back(Action(false, true, getElapsedTime()));
    break;
  default:
    break;
  }
}

void GameManager::notifyKeyReleased(const sf::Keyboard::Key &key) {
  if (isRunning()) {
    switch (key) {
    case sf::Keyboard::Space:
      getPlayer(0).stopJump();
      m_state.m_all_actions.push_back(Action(true, false, getElapsedTime()));
      break;
    case sf::Keyboard::Down:
    case sf::Keyboard::S:
      getPlayer(0).stopRoll();
      m_state.m_all_actions.push_back(Action(false, false, getElapsedTime()));
      break;
    default:
      break;
    }
  }
}

void GameManager::notifyEvent(const sf::Event &event) {
  switch (event.type) {
  case sf::Event::KeyPressed:
    notifyKeyPressed(event.key.code);
    break;
  case sf::Event::KeyReleased:
    notifyKeyReleased(event.key.code);
    break;
  case sf::Event::Resized:
    m_window->setView(
        sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
    invalidateDisplay();
  default:
    break;
  }
}

TimeManager &GameManager::getTimeManager() { return m_time_manager; }

void GameManager::setTimeManager(const TimeManager &timeManager) {
  if (isAlive()) {
    throw std::logic_error(
        "Cannot change the time manager when a game is started");
  }
  m_time_manager = timeManager;
}

void GameManager::addGameQuitListener(const OnGameQuitListener &listener) {
  m_game_quit_listeners.push_back(listener);
}
void GameManager::addGameOverListener(const OnGameOverListener &listener) {
  m_game_over_listeners.push_back(listener);
}
void GameManager::addTickListener(const OnTickListener &listener) {
  m_tick_listeners.push_back(listener);
}
void GameManager::addManagerLaunchedListener(
    const OnManagerLaunchedListener &listener) {
  m_manager_launched_listener.push_back(listener);
}

#ifdef _TEST
void GameManager::stopRunning() { m_state.m_player_running = false; }
void GameManager::runInFastMode() { m_fast_mode = true; };
#endif

void GameManager::resetGame(unsigned int seed /*=0*/,
                            unsigned int num_players /*=1*/) {
  stopGame();
  m_state = GameState(*this, seed, num_players);
  invalidateDisplay();
}

void GameManager::saveReplay() {
  Action::saveReplayList(m_state.m_all_actions, m_state.m_seed);
}

void GameManager::executeAction(Action action) {
  if (action.m_is_jump) {
    if (action.m_is_press) {
      getPlayer(0).startJump();
    } else {
      getPlayer(0).stopJump();
    }
  } else {
    if (action.m_is_press) {
      getPlayer(0).startRoll();
    } else {
      getPlayer(0).stopRoll();
    }
  }
}

void GameManager::watchReplay() {
  unsigned int seed;
  std::vector<Action> all_actions = Action::loadReplay(&seed);
  resetGame(seed, 1);
  m_state.m_all_actions = std::move(all_actions);
  disableInput();
  startGame(true);
}

bool GameManager::isInReplayMode() { return m_state.m_replaying; }

void GameManager::launchManager() {
  if (m_launched) {
    throw std::logic_error("This GameManager is already launched.");
  }
  m_launched = true;

  for (const OnManagerLaunchedListener &listener :
       m_manager_launched_listener) {
    listener(*this);
  }

  manager_loop();
}

void GameManager::quitGame() noexcept {
  stopGame();
  m_launched = false;
}

void GameManager::attachWindow(GameWindow &window) {
  m_window = OptionalRef<GameWindow &>(window);
  invalidateDisplay();
}

void GameManager::detachWindow() { m_window = OptionalRef<GameWindow &>(); }

bool GameManager::isInputEnabled() const { return m_input_enabled; }

void GameManager::enableInput() { m_input_enabled = true; }

void GameManager::disableInput() { m_input_enabled = false; }
bool GameManager::willCloseWindowOnQuit() const { return m_close_on_quit; }
void GameManager::setCloseWindowOnQuit(bool closeOnQuit) {
  m_close_on_quit = closeOnQuit;
}
